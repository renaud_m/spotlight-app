import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'

import illustration from '@assets/images/not-found-illustration.svg'

import Button from '@superGlobals/Button'

import AppContext from '@context/AppContext'

const NotFound = (props) => {
    const history = useHistory()
    const context = useContext(AppContext)

    /*(⩾﹏⩽) 404 (⩾﹏⩽)*/ 

    return (
        <div className="w-100 h-100-vh flex-column items-center justify-center">
            <div className="mw-25-vw mb-l">
                <img src={illustration} alt="Page introuvable" />
            </div>

            <h1 className="font-hero">404</h1>
            <h2 className="font-l-400 mb-l">Page introuvable</h2>

            <Button
                importance="primary"
                text="Retour à l'accueil"
                onClick={() => context.user.isLogged ? history.push('/list/all') : history.push('/signin')}
            />
        </div>
    )
}
    
export default NotFound;