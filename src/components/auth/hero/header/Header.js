import React from 'react'

import Logo from './Logo'

const Header = (props) => (
    <header className="position-absolute p-m m:p-xl">
        <div className="flex-row justify-start">
            <Logo />
            <h1 className="font-l-900 text-uppercase color-primary">Spotlight</h1>
        </div>
    </header>
)

export default Header