import React from 'react'

const Logo = (props) => (
<svg className="mr-s" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
  <defs>
    <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stopColor="#34a6fd"/>
      <stop offset="1" stopColor="#3434fd"/>
    </linearGradient>
  </defs>
  <g id="Logo" transform="translate(0.391 0.391)">
    <circle data-name="Ellipse 57" cx="16" cy="16" r="16" transform="translate(-0.391 -0.391)" fill="url(#linear-gradient)"/>
    <path data-name="Tracé 46" d="M16.075,7.9a8.036,8.036,0,0,1-15,4.013H4.493a5.363,5.363,0,0,0,8.733-2.675H.112A7.911,7.911,0,0,1,0,7.9,8.036,8.036,0,0,1,15,3.886H11.582A5.364,5.364,0,0,0,2.848,6.562H15.964A8,8,0,0,1,16.075,7.9Z" transform="translate(7.747 7.888)" fill="#fff"/>
  </g>
</svg>
)

export default Logo