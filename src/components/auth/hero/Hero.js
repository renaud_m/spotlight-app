import React from 'react'
import Illustration from '@assets/images/auth-illustration.svg'

const Hero = () => (
    <div className="w-100 h-100 flex-row justify-center">
        <img src={Illustration} alt="Illustration" className="mt-l mw-30-vw" />
    </div>
)

export default Hero