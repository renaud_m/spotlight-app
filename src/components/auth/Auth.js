import React from 'react';
import { Router, Switch, Route } from 'react-router-dom'
import history from '@history';

import Hero from './hero/Hero'
import Header from './hero/header/Header'

import SignIn from './signin/SignIn'
import SignUp from './signup/SignUp'

import NotFound from '../notFound/NotFound'

const Auth = (props) => {
	const generateLayout = (component) => (
		<>
			<Header />
			<section className="hidden m:block col-12 xl:col-8 m:col-6 p-l">
				<Hero />
			</section>
			<div className="col-12 xl:col-4 m:col-6 p-m flex-column justify-center items-center stretched-v bg-white">
				{component}
			</div>
		</>
	)
	
	return (
		<div className="w-100 h-100-vh auth-page">
			<div className="grid w-100 h-100">
				<Router history={history}>
					<Switch>
						<Route path="/signin">
							{generateLayout(<SignIn />)}
						</Route>
						<Route path="/signup">
							{generateLayout(<SignUp />)}
						</Route>
						<Route component={NotFound} />
					</Switch>
				</Router>
			</div>
		</div>
	)
}

export default Auth