import React, { useState, useContext } from 'react'
import axios from 'axios'

import { useHistory } from 'react-router-dom'

import ForgetPasswordLink from './elements/ForgetPasswordLink'

import TextField from '@superGlobals/TextField'
import Button from '@superGlobals/Button'

import AppContext from '@context/AppContext'

const SignIn = () => {
	const history = useHistory()
	const context = useContext(AppContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const [waiting, setWaiting] = useState(false)

	const [textfieldsStatus, setTextfieldsStatus] = useState({
		email: 'normal',
		password: 'normal',
	})

	const handleSubmit = (event) => {
		event.preventDefault()

		setWaiting(true)

		let url = process.env.REACT_APP_API_URL + '/signin'

		axios.post(url, {email: email, password: password,}).then(response => {
			setTextfieldsStatus({
				email: 'normal',
				password: 'normal',
			})
			localStorage.setItem('spotlight-token', response.data.data.token)
			context.user.toggleStatus(true)
			history.push('/list/all')
		}).catch(error => {
			setTextfieldsStatus(
				error.response.data.message.toLowerCase().includes('email') ?
				{ ...textfieldsStatus, email: 'error' }
				:
				{ ...textfieldsStatus, password: 'error' }
			)

			setWaiting(false)
		})
	}

	const form = (
		<div className="w-100">
			<h1 className="font-l-700 uppercase align-center">Connectez-vous</h1>
			<h2 className="font-m-400 align-center mb-m">À votre compte</h2>

			<form className="flex-column items-center" onSubmit={handleSubmit}>
				<div className="flex-column w-100">
					<TextField
						tagName="input"
						label="Email"
						type="email"
						name="email"
						className="mb-m"
						onChange={event => setEmail(event.target.value)}
						status={textfieldsStatus.email}
						errorMessage="Email invalide"
						required
					/>
					<TextField
						tagName="input"
						className="mb-m"
						label="Mot de passe"
						type="password"
						name="password"
						onChange={event => setPassword(event.target.value)}
						status={textfieldsStatus.password}
						errorMessage="Mot de passe invalide"
						required
					/>
				</div>

				<ForgetPasswordLink />

				<Button
					importance="primary"
					className="stretched-h"
					text="Se connecter"
					waiting={waiting}
					type="submit"
				/>
			</form>
		</div>
	)

	return form
}

export default SignIn