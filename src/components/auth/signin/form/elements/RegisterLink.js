import React from 'react';
import { Link } from 'react-router-dom'

function RegisterLink() {    
    return (
        <div className="flex-row justify-center wrap mt-m">
            <p className="text mh-t">Pas encore de compte</p>
            <Link to="/signup" className="link">S'enregistrer</Link>
        </div>
    )
}
    
export default RegisterLink;