import React from 'react';
import { Link } from 'react-router-dom';

function ForgetPasswordLink() {
    return (
    	<Link to="/signin" className="color-primary font-500 mb-m" href="#">Mot de passe oublié ?</Link>
    )
}

export default ForgetPasswordLink;