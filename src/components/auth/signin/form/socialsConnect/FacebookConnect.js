import React from 'react'
import styled from 'styled-components'

import { FaFacebookF } from "react-icons/fa";

const FacebookBtn = styled.button`
    background-color: #3b5998;
    color: #fff;

    &:hover { background-color: #324b80 }
`

const FacebookConnect = () => {
    return (
        <div className="mh-s mb-m">
            <FacebookBtn className="button">
                <FaFacebookF className="mr-s font-m" />
                <span>Avec Facebook</span>
            </FacebookBtn>
            
       </div>
    )
}
    
export default FacebookConnect;