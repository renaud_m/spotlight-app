import React from 'react'

import Form from './form/Form'
import RegisterLink from './form/elements/RegisterLink'

const SignIn = (props) => (
    <>
        <Form />
        <RegisterLink />
    </>
)

export default SignIn