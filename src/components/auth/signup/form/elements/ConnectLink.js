import React from 'react'
import { Link } from 'react-router-dom'

const ConnectLink = () => (
    <div className="flex-row justify-center wrap mt-m">
        <p className="text mh-t">Déja enregistré ?</p>
        <Link to="/signin" className="link">Se connecter</Link>
    </div>
)
    
export default ConnectLink