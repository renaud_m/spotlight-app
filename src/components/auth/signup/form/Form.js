import React, { useState, useContext } from 'react'
import axios from 'axios'

import { useHistory } from 'react-router-dom'

import TextField from '@superGlobals/TextField'
import Button from '@superGlobals/Button'

import AppContext from '@context/AppContext'

const Form = (props) => {
	let history = useHistory()
	const context = useContext(AppContext)

	const [formDatas, setFormDatas] = useState({
		firstname: '',
		lastname: '',
		username: '',
		email: '',
		password: '',
		buttonValue: 'S\'inscrire',
		waiting: false,
	})

	const handleSubmit = (event) => {
		event.preventDefault()

		setFormDatas({
			...formDatas,
			waiting: true,
		})

		let url = process.env.REACT_APP_API_URL + '/signup'

		axios.post(url,
			{
				firstname: formDatas.firstname,
				lastname: formDatas.lastname,
				username: formDatas.username,
				email: formDatas.email,
				password: formDatas.password,
			}
		).then(response => {
				localStorage.setItem('spotlight-token', response.data.data.token);
				context.user.toggleStatus('login')
				history.push('/list/all')
			}
		).catch(error => {
			setFormDatas({
				...formDatas,
				waiting: false,
			})
		})
	}

	const handleChange = (event) => {
		let target = event.target.name

		setFormDatas({
			...formDatas,
			[target]: event.target.value
		})
	}

	return (
		<div className="w-100">
			<h1 className="font-l-700 align-center uppercase">Inscrivez-vous</h1>
			<h2 className="font-m-400 align-center mb-m">Maintenant</h2>

			<form className="flex-column items-center" onSubmit={handleSubmit}>
				<div className="flex-column w-100">
					<div className="flex-row mbox-rm mb-m">
						<TextField
							tagName="input"
							label="Prénom*"
							type="text"
							name="firstname"
							onChange={handleChange}
							className="w-100"
							required
						/>
						<TextField
							tagName="input"
							label="Nom*"
							type="text"
							name="lastname"
							onChange={handleChange}
							className="w-100"
							required
						/>
					</div>
					<TextField
						tagName="input"
						label="Nom d'utilisateur*"
						type="text"
						name="username"
						className="mb-m"
						onChange={handleChange}
						required
					/>

					<TextField
						tagName="input"
						label="Email*"
						type="email"
						name="email"
						className="mb-m"
						onChange={handleChange}
						required
					/>
					<TextField
						tagName="input"
						className="mb-m"
						label="Mot de passe*"
						type="password"
						name="password"
						onChange={handleChange}
						required
					/>
				</div>

				<Button
					importance="primary"
					className="stretched-h"
					text={formDatas.buttonValue}
					waiting={formDatas.waiting}
					type="submit"
				/>
			</form>
		</div>
	)
    
}

export default Form;