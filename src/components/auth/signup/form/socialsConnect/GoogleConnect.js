import React from 'react'
import styled from 'styled-components'

import { FaGoogle } from "react-icons/fa";

const GoogleBtn = styled.button`
    background-color: #4285f4;
    color: #fff;

    &:hover { background-color: #3879e5 }
`

const GoogleConnect = () => {
    return (
        <div className="mh-s mb-m">
            <GoogleBtn className="button">
                <FaGoogle className="mr-s font-m" />
                <span>Avec Google</span>
            </GoogleBtn>
       </div>
    )
}
    
export default GoogleConnect;