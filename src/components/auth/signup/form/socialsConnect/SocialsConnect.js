import React from 'react';

import FacebookConnect from './FacebookConnect.js';

const SocialsConnect = () => (
    <div className="mb-m">
        <div className="flex-row justify-center wrap">
            <FacebookConnect />
        </div>

        <div className="align-center">
            <span>Où</span>
        </div>
    </div>
)
    
export default SocialsConnect;