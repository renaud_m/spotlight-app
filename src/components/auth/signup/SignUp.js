import React from 'react';

import Form from './form/Form';
import ConnectLink from './form/elements/ConnectLink'

function SignUp() {
	return (
		<>
			<Form />
			<ConnectLink />
		</>
	)
}

export default SignUp