import React from 'react'
import { MdClearAll } from 'react-icons/md'

import { NavLink } from 'react-router-dom'

import { Container } from '../global/Styles'

const Hmenu = (props) => {
    const menuItems = [
        {
            icon: <MdClearAll className="mr-s" />,
            text: 'Groupes',
            path: '/'
        }
    ]

	return (
        <nav className="bg-white shadow-t mb-l">
            <Container className="position-relative w-100">
                <ul className="flex-row justify-start w-100">
                {
                    menuItems.map((item, index) => (
                        <li key={index}>
                            <NavLink
                                to={item.path}
                                className="flex-row justify-start items-center ink-clear ph-m pv-s"
                                activeClassName="color-primary font-700"
                                children={
                                    <>  
                                        {item.icon}
                                        <span>{item.text}</span>
                                    </>
                                }

                                isActive={() => <span>ok</span>}
                            />
                        </li>
                    ))
                }
                </ul>
            </Container>
        </nav>
	)
}


export default Hmenu