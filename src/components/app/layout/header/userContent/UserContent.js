import React from 'react'

import User from './user/User'

const Partials = (props) => (
	<div className="flex-row justify-end items-center mbox-rl ml-auto">
        <User navbar={props.navBar} />
    </div>
)

export default Partials