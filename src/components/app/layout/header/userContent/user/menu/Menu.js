import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { MdArrowDropDown, MdPerson } from 'react-icons/md'

import MenuDialog from '@superGlobals/MenuDialog'
import RippleSurface from '@superGlobals/RippleSurface'

import Logout from './Logout'

const NoDecorationsLink = styled(
    ({ component, ...props }) => React.cloneElement(component, props)
)`text-decoration: none`

const Menu = (props) => {
    const menuItems = [
        {
            text: 'Mon compte',
            icon: <MdPerson />,
            path: '/dashboard'
        }
    ]

    const content = menuItems.map((item, index) => (
        <NoDecorationsLink className="color-black" key={index}
            component={
                <Link to={item.path}>
                    <RippleSurface mode="dark" className="flex-row justify-center items-center hover-bg-wd font-700-m p-s">
                        {item.icon}
                        <span className="font-700 ml-s">{item.text}</span>
                    </RippleSurface>
                </Link>
            }
        />
    ))

	return (
        <MenuDialog
            trigger={
                <RippleSurface mode={props.navBar ? 'dark md-circle-dark' : 'light md-circle-light'}>
                    <MdArrowDropDown className="cursor-pointer" />
                </RippleSurface>
            }
            content={
                <>
                    {content}
                    <Logout />
                </>
            }
            className="flex align-center"
        />
	)
}

export default Menu