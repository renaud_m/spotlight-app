import React, { useContext } from 'react'
import { MdExitToApp } from 'react-icons/md'

import { useHistory } from 'react-router-dom'

import RippleSurface from '@superGlobals/RippleSurface'

import AppContext from '@context/AppContext'

const Menu = (props) => {
    const history = useHistory()
    const context = useContext(AppContext)

    const handleClick = () => {
        localStorage.removeItem('spotlight-token')
        context.user.toggleStatus(false)
        history.push('/signin')
    }

	return (
        <RippleSurface mode="dark" onClick={handleClick} className="flex-row justify-center items-center hover-bg-wd font-700-m p-s color-black">
            <MdExitToApp />
            <span className="font-700 ml-s">Déconnexion</span>
        </RippleSurface>
	)
}

export default Menu