import React from 'react'

const Thumbnail = (props) => {
    const name = props.data.firstname[0].toUpperCase() + props.data.lastname[0].toUpperCase()

	return (
        <div
            className="rounded-full flex-row justify-center items-center bg-secondary color-secondary-c p-t mr-s"
            children={name}
        />
	)
}

export default Thumbnail