import React from 'react'

const UserData = (props) => (
    <div className="flex">
        <span className="font-700">{props.data.firstname + ' ' + props.data.lastname}</span>
    </div>
)

export default UserData