import React, { useState, useEffect } from 'react'
import axios from 'axios'

import Thumbnail from './Thumbnail'
import UserData from './UserData'

const Infos = () => {
	const [content, setContent] = useState()

	useEffect(() => {
		const token = localStorage.getItem('spotlight-token')
		const headers = { headers: { Authorization: `Bearer ${token}` } }
		
		axios.get('http://localhost:3333/user/getDatas', headers).then(response => {
			setContent(
				<>
					<Thumbnail data={response.data.data} />
					<UserData data={response.data.data} />
				</>
			)
		})
	}, [])

	return (
		<div className="flex-row justify-center items-center mr-s">
			{content}
		</div>
	)
}

export default Infos