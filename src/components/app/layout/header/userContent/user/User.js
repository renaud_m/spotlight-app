import React from 'react'

import Infos from './infos/Infos'
import Menu from './menu/Menu'

const User = (props) => (
	<div className="flex-row justify-center items-center">
		<Infos />
		<Menu navbar={props.navBar} />
	</div>
)

export default User