import React from 'react'

import {MdNotificationsNone} from "react-icons/md"

const Notifications = () => <MdNotificationsNone />

export default Notifications