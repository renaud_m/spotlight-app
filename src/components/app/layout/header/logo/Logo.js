import React from 'react'
import styled from 'styled-components'

import { Link } from 'react-router-dom'

const Icon = styled.svg`
    max-width: 28px;
    width: 100%;
`

const Logo = (props) => (
    <div className="flex-row justify-start items-center hidden m:flex">
        <Link to="/" className="ink-clear flex-row items-center color-white">
            <Icon xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 36 36">
                <defs>
                    <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                    <stop offset="0" stopColor="#34a6fd"/>
                    <stop offset="1" stopColor="#3434fd"/>
                    </linearGradient>
                </defs>
                <g id="Content" transform="translate(57.698 -22)">
                    <g id="Logo" transform="translate(-57.698 22)">
                    <circle cx="18" cy="18" r="18" transform="translate(0)" fill="url(#linear-gradient)"/>
                    <path d="M18.331,9.023A9.163,9.163,0,0,1,1.226,13.6h3.9a6.115,6.115,0,0,0,9.959-3.05H.128A9.021,9.021,0,0,1,0,9.023,9.163,9.163,0,0,1,17.1,4.451h-3.9A6.116,6.116,0,0,0,3.247,7.5H18.2A9.121,9.121,0,0,1,18.331,9.023Z" transform="translate(8.834 8.975)" fill="#fff"/>
                    </g>
                </g>
            </Icon>
            <h1 className="hidden s:flex font-l-900 uppercase ml-s">Spotlight</h1>
        </Link>
    </div>
)

export default Logo

