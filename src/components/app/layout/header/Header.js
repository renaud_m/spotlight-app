import React from 'react'

import Logo from './logo/Logo'
import UserContent from './userContent/UserContent'

import styled from 'styled-components'

import { Container } from '../global/Styles'

const Sizing = styled.header`
	width: ${props => props.navBar ? `calc(100% - 5em)` : `100%`}
`


const Header = (props) => (
    <Sizing navbar={props.navBar} className={'h-5-em flex-row justify-end items-center' + (props.navBar ? ' position-fixed-tr bg-white shadow-t' : ' bg-primary color-primary-c')}>
        <Container className="flex-row justify-center items-center m:justify-end w-100 ph-m">
            { !props.navBar && <Logo /> }
            <UserContent navbar={props.navBar} />
        </Container>
    </Sizing>
)

export default Header