import styled from 'styled-components'

export const H80 = styled.div`height: 80px`

export const W80 = styled.div`width: 80px`

export const G80 = styled.div`
    width: 80px;
    height: 80px;
`

export const Container = styled.div`
    max-width: 1440px;
    width: 100%;
    margin-left: auto;
    margin-right: auto;
`

export const AppContainer = styled.main`
    padding: 40px;
    margin-left: 80px;
    margin-top: 80px;
    transition: margin-left .3s ease;

    @media screen and (max-width: 768px) {
        padding: 20px;
        margin-left: 0;
    }
`

export const HomeContainer = styled.main`
    margin-left: auto;
    margin-right: auto;
    padding-left: 20px;
    padding-right: 20px;
    width: 100%;
    max-width: 1440px
`