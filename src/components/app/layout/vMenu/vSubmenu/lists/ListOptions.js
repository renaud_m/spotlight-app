import React from 'react'
import { MdMoreHoriz } from 'react-icons/md'

import MenuDialog from '@superGlobals/MenuDialog'
import RippleSurface from '@superGlobals/RippleSurface'
import Delete from './Delete'

const ListOptions = (props) => (
    <MenuDialog
        type="confirm"
        trigger={
            <RippleSurface className="md-circle-light" mode="light">
                <MdMoreHoriz />
            </RippleSurface>
        }
        content={<Delete listId={props.listId}/>}
        className="position-absolute-vr mh-m"
    />
)


export default ListOptions