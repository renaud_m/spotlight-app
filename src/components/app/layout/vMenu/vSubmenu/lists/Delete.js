import React, { useContext, useState } from 'react'
import { MdDelete } from 'react-icons/md'
import axios from 'axios'

import { Redirect, useParams } from 'react-router';

import AppContext from '@context/AppContext'
import RippleSurface from '@superGlobals/RippleSurface'
import Spinner from '@superGlobals/Spinner'

const Delete = (props) => {
    const [redirect, setRedirect] = useState(false)
    const context = useContext(AppContext)

    let routeParams = useParams()
    let currentList = parseInt(routeParams.id)
    
    const handleCLick = () => {
        let params = {
            type: 'confirm',
            callBack: () => {
                context.app.setModal(true, 'Suppression en cours', <Spinner size="64" color="black" />, {type: 'alert'} )

                axios.delete('http://localhost:3333/list/delete/' + props.listId).then((response) => {
                    context.app.insertApiResponse(response.data.query_name)
                    context.app.setModal(false)

                    if (currentList === props.listId) setRedirect(true)
                })
            }
        }

        context.app.setModal(
            true,
            'Êtes-vous sûr(e) ?',
            <span>L'ensemble des donnée seront supprimées définitivements</span>,
            params
        )
    }
    
    return (redirect) ? <Redirect to="/list/all" /> : (
        <RippleSurface mode="dark" className="flex-row p-s hover-bg-wd color-black cursor-pointer" onClick={handleCLick}>
            <MdDelete className="mr-s" />
            <span className="font-m-700">Supprimer</span>
        </RippleSurface>
    )
}


export default Delete 