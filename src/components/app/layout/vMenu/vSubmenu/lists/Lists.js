import React, { useState, useEffect, useRef, useContext } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'

import { useParams } from 'react-router-dom'

import RippleSurface from '@superGlobals/RippleSurface'

import styled from 'styled-components'

import AppContext from '@context/AppContext'

import ListOptions from './ListOptions'

const MenuItem = styled(NavLink)`
    text-decoration: none;

    & * { text-decoration: none }
`

const Lists = (props) => {
    const [lists, setLists] = useState([])
    const menuLinks = useRef()
    const context = useContext(AppContext)
    const params = useParams()

    useEffect(() => {
        const token = localStorage.getItem('spotlight-token')
        const headers = { headers: { Authorization: `Bearer ${token}` } }
        
        axios.get('http://localhost:3333/list/all/' + params.groupId, headers).then(response => {            
            setLists(response.data.data)
        })
    }, [context.app.lastApiResponse, params.groupId])
    
    return (
        lists.map(list => (
            <div className="position-relative" key={list.id}>
                <RippleSurface mode="light" transfert>
                    <MenuItem
                        to={{ pathname: '/list/' + params.groupId + '/' + list.id, state: list.id }}
                        className="color-secondary-c flex p-m hover-bg-sl h-5-em"
                        activeClassName="bg-secondary-l font-m-700"
                        ref={menuLinks}
                        children={list.name}
                    />
                </RippleSurface>
                <ListOptions listId={list.id} />
            </div>
        ))
    )
}


export default Lists