import React from 'react'
import styled from 'styled-components'

import { MdArrowBack } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'

import Lists from './lists/Lists'
import AddList from './addList/AddList'

const Submenu = styled.div`
    position: absolute;
    top: 0;
    left: 5em;
    height: 100%;
    width: 260px;
    z-index: -1;
    transform: ${props => props.isSubmenuOpen ? `translateX(0%)` : `translateX(-100%)`};
    transition: transform .3s ease;

    @media screen and (max-width: 768px) {
        position: fixed;
        top: 0;
        left: 0;
        height: calc(100% - 5em);
        width: 100%;
        z-index: -1;
        transform: ${props => props.isSubmenuOpen ? `translateX(0%)` : `translateX(-100%)`};
        transition: transform .3s ease;
    }
`

const VSubmenu = (props) => (
    <Submenu isSubmenuOpen={props.isSubmenuOpen} className="bg-secondary color-secondary-c flex-column">
        <div className="h-5-em border-v-sl flex-row justify-between items-center ph-m">
            <span className="font-m-700 text-uppercase">{props.name}</span>

            <RippleSurface
                mode="light"
                className="md-circle"
                onClick={() => props.setIsSubmenuOpen(false)}
                children={<MdArrowBack />}
            />
        </div>
        <div className="content">
            <Lists />
        </div>

        <AddList />
    </Submenu>
)


export default VSubmenu