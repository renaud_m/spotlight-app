import React, { useContext, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'

import TextField from '@superGlobals/TextField'
import Button from '@superGlobals/Button'

import AppContext from '@context/AppContext'

const AddModal = (props) => {
    const params = useParams()
    const context = useContext(AppContext)
    const [name, setName] = useState()

    const handleSubmit = (event) => {
        event.preventDefault()

        if (name !== undefined) {
            const token = localStorage.getItem('spotlight-token')
            
            const data =  { name: name }
            const headers = { headers: { Authorization: `Bearer ${token}` } }

            axios.post('http://localhost:3333/list/add/' + params.groupId, data, headers)
                .then(response => {
                    context.app.insertApiResponse(response.data.query_name)
                    context.app.setModal(false)
                })
                .catch(error => console.log(error))
        }
    }
    
    return (
        <form onSubmit={handleSubmit}>
            <TextField
                label="Nom de la liste"
                onChange={event => setName(event.target.value)}
                className="mb-m"
            />
            <Button
                importance="primary"
                text="Ajouter la liste"
                type="submit"
            />
        </form>
    )
}


export default AddModal