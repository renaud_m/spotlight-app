import React, { useContext } from 'react'
import { MdAddCircle } from "react-icons/md"
import AppContext from '@context/AppContext'

import AddModal from './AddModal'

import RippleSurface from '@superGlobals/RippleSurface'

const AddList = (props) => {
    const context = useContext(AppContext)
    
    return (
        <RippleSurface transfert>
            <div
                className="h-5-em flex-row justify-start items-center p-m mt-auto hover-bg-sl border-v-sl"
                onClick={() => context.app.setModal(true, 'Ajouter une liste', <AddModal />)}>
                
                <MdAddCircle />
                <span className="font-m-700 ml-s">Ajouter une liste</span>
            </div>
        </RippleSurface>
    )
}


export default AddList