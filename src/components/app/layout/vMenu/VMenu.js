import React, { useState } from 'react'

import { AiFillDatabase } from 'react-icons/ai'

import VSubmenu from './vSubmenu/VSubmenu'
import Logo from './logo/Logo'

import RippleSurface from '@superGlobals/RippleSurface'
import Tooltip from '@superGlobals/Tooltip'

const VMenu = (props) => {
    const [isSubmenuOpen, setIsSubmenuOpen] = useState(false)

    return (
        <>
            <ul className="position-relative flex-row justify-center m:flex-column m:justify-start h-100 gradient-ppd">
                <div className="hidden m:flex w-100 h-5-em cursor-pointer">
                    <Logo />
                </div>

                <Tooltip
                    children={
                        <RippleSurface onClick={() => setIsSubmenuOpen(isSubmenuOpen ? false : true)}>
                            <div className="flex-row justify-center items-center hover-bg-pd h-5-em cursor-pointer">
                                <AiFillDatabase className="color-primary-c"/>
                            </div>
                        </RippleSurface>
                    }
                    type="big"
                    direction="right"
                    title="Listes"
                    className="w-100"
                />
            </ul>
            <VSubmenu name="Listes" isSubmenuOpen={isSubmenuOpen} setIsSubmenuOpen={setIsSubmenuOpen} />
        </>
    )
}

export default VMenu