import React from 'react'
import styled from 'styled-components'

import { Link } from 'react-router-dom'

const Icon = styled.svg`
    fill: #fff;
    max-width: 36px;
    width: 100%;
`

const Logo = () => (
    <Link to="/" className="flex-row justify-center items-center w-100">
        <Icon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36">
            <path d="M18,0A18,18,0,1,0,36,18,18,18,0,0,0,18,0Zm0,27.17a9.17,9.17,0,0,1-8-4.6H14a6.11,6.11,0,0,0,10-3.05H9A9.64,9.64,0,0,1,8.83,18a9.16,9.16,0,0,1,17.1-4.58H22A6.13,6.13,0,0,0,13.4,14a6.25,6.25,0,0,0-1.32,2.49H27A9.89,9.89,0,0,1,27.17,18,9.17,9.17,0,0,1,18,27.17Z"/>
        </Icon>
    </Link>
)

export default Logo