import React from 'react'

import Header from './header/Header'
import VMenu from './vMenu/VMenu'
import Hmenu from './hmenu/Hmenu'

import styled from 'styled-components'

const Nav = styled.nav`

	@media screen and (max-width: 768px) {
		width: 100%;
		height: auto;
		bottom: 0;
    	top: initial;
	}
`

const Layout = (props) => (
	<>
		<Header navBar={props.navBar} />
		{
			(props.navBar)
			?
			<Nav className="w-100 m:w-5-em flex-column-between-v position-fixed-tl h-100">
				<VMenu />
			</Nav>
			:
			<Hmenu />
		}
	</>
)

export default Layout