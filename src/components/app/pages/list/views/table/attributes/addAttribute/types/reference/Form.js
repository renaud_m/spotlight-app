import React, { useEffect, useState, useContext } from 'react'

import { useParams } from 'react-router-dom'
import axios from 'axios'

import AppContext from '@context/AppContext'

import Select from '@superGlobals/Select'
import Button from '@superGlobals/Button'

const Form = (props) => {
    const params = useParams()
    const context = useContext(AppContext)

    const [listsPicker, setListsPicker] = useState()
    const [selectedList, setSelectedList] = useState()

    useEffect(() => {
        let url = process.env.REACT_APP_API_URL + '/list/getAvailableForReference/'+ params.groupId + '/' + props.listId

        axios.get(url).then(response => {
            let items = []

            for (let i = 0; i < response.data.data.length; i++) {
                const list = response.data.data[i]

                items.push( { value: list.id, text: list.name })
            }

            
            setListsPicker(
                <>
                    <h2 className="mb-s">Choisissez votre liste de référence</h2>
                    <Select
                        items={items}
                        selected={{text: 'Choisissez une liste', value: ''}}
                        onChange={table => setSelectedList(table)}
                    />
                </>
            )
        })
    }, [props.listId, params.groupId, props.id])

    const handleClick = () => {
        let url = process.env.REACT_APP_API_URL + '/list/' + props.listId + '/reference/add'

        axios.post(url, { 'pivotListId': selectedList}).then(response => {
            context.app.insertApiResponse(response.data.query_name)
            context.app.setModal(false)
        })
    }

    return (
        <>
            <div className="mb-m">
                {listsPicker}
            </div>
            {
                selectedList
                &&
                <Button
                    importance="primary"
                    text="Insérer la référence"
                    onClick={handleClick}
                />
            }
        </>
    )
}

export default Form