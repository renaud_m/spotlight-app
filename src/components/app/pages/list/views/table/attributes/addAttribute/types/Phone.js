import React, { useContext } from 'react'
import axios from 'axios'

import AppContext from '@context/AppContext'
import RippleSurface from '@superGlobals/RippleSurface'

import TypedIcon from '@lib/utils/TypedIcon'

const Phone = (props) => {
    const context = useContext(AppContext)

    const handleClick = () => {
        let url = process.env.REACT_APP_API_URL + '/list/attribute/add/' + props.listId + '/phone'
		axios.post(url).then((response) => context.app.insertApiResponse(response.data.query_name))
    }

    return (
        <RippleSurface mode="dark" onClick={handleClick} className="hover-bg-wd">
            <div className="flex-row justify-start items-center p-s">
                <TypedIcon type="phone" className="mr-s" />
                <span className="font-700">Téléphone</span>
            </div> 
        </RippleSurface>
    )
}

export default Phone