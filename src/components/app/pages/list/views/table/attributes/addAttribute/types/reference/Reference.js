import React, { useContext } from 'react'
import { MdLocationSearching } from 'react-icons/md'

import AppContext from '@context/AppContext'
import RippleSurface from '@superGlobals/RippleSurface'

import Form from './Form'

const Reference = (props) => {
    const context = useContext(AppContext)

    return (
        <RippleSurface mode="dark" onClick={() => context.app.setModal(true, 'Référence', <Form listId={props.listId} />)} className="hover-bg-wd">
            <div className="flex-row justify-start items-center p-s">
                <MdLocationSearching className="mr-s" />
                <span className="font-700">Référence</span>
            </div>
        </RippleSurface>
    )
}

export default Reference