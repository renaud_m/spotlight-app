import React from 'react'
import { MdAddCircle } from 'react-icons/md';

import AddAttributeItems from './AddAttributeItems'
import MenuDialog from '@superGlobals/MenuDialog'

import RippleSurface from '@superGlobals/RippleSurface'

const AddAttributeCol = (props) => (
    <th className="h-100">
        <MenuDialog
            title="Type d'attribut"
            trigger={
                <RippleSurface mode="light" className="w-100 h-100 cursor-pointer flex-row justify-center items-center">
                    <MdAddCircle className="skr-icon" />
                </RippleSurface>
            }
            content={<AddAttributeItems listId={props.listId} />}
            className="h-100"
            contentClassName="p-m color-black"
        />
    </th>
)

export default AddAttributeCol