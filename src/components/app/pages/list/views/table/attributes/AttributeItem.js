import React, { useState, useRef } from 'react'
import styled from 'styled-components'

import { MdGrain } from 'react-icons/md'
import { Draggable } from 'react-smooth-dnd'

import AttributeOptions from './attributeOptions/AttributeOptions'
import EditableField from '@superGlobals/EditableField'

import TypedIcon from '@lib/utils/TypedIcon'

const Row = styled.th`
	width: ${props => props.width + 'px'}
`

const AttributeItem = (props) => {
	const row = useRef()

	const [width, setWidth] = useState(props.width ? props.width : 'auto')

	return (
		<Draggable render={() => (
				<Row width={width} ref={row}>
					<div className="row-inner ph-s">
					<div className="grab-col"><MdGrain className="skr-icon grain" /></div>
						<div className="h-100 flex-row justify-center items-center mh-m">
							<TypedIcon type={props.type} className="mr-t" />
		
							<EditableField
								tag="span"
								value={props.name}
								mode="light"
								dataTable="attributes"
								datafield="name"
								dataId={props.id}
							/>
		
						</div>
		
						<AttributeOptions id={props.id} listId={props.listId} />
					</div>
					
				</Row>
			)} 
		/>
	)
}

export default AttributeItem