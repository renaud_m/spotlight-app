import React from 'react'
import styled from 'styled-components'

import { useParams } from 'react-router-dom'

import Text from './types/Text'
import Number from './types/Number'
import Label from './types/Label'
import Percentage from './types/Percentage'
import Link from './types/Link'
import Phone from './types/Phone'
import Email from './types/Email'
import Date from './types/Date'
import Reference from './types/reference/Reference'

const ItemsContainer = styled.div`max-height: 250px`

const AddAttributeItems = (props) => {
    const params = useParams()

    return (
        <div className="color-black">
            <ItemsContainer className="p-s overflow-y-auto">
                <Text listId={params.listId} />
                <Number listId={params.listId} />
                <Label listId={params.listId} />
                <Percentage listId={params.listId} />
                <Link listId={params.listId} />
                <Phone listId={params.listId} />
                <Email listId={params.listId} />
                <Date listId={params.listId} />
                <Reference listId={params.listId} />
            </ItemsContainer>
        </div>
    )
}

export default AddAttributeItems