import React, { useState, useEffect } from 'react'
import axios from 'axios'
import styled from 'styled-components'

const Resize = styled.div`
	position: absolute;
    top: 0;
    right: -2px;
    width: 2px;
    height: 100%;
	z-index: 300;
    cursor: col-resize;
`

const AttributeItem = (props) => {
	const [isResizing, setIsResizing] = useState(false)

	const [initialPagX, setInitialPageX] = useState(0)
	const [initialRowWidth, setInitialRowWidth] = useState(0)

	const handleMouseDown = (event) => {
		setIsResizing(true)
		setInitialPageX(event.pageX)

		let rowData = props.row.current.getBoundingClientRect()
		setInitialRowWidth(rowData.width)
	}

	useEffect(() => {
		const handleMouseMove = (event) => {
			let diffX = event.pageX - initialPagX
			props.setWidth(initialRowWidth + diffX)
		}

		const handleMouseUp = () => {
			setIsResizing(false)
			document.removeEventListener('mousemove', handleMouseMove, true)

			const url = process.env.REACT_APP_API_URL + '/list/value/edit/attributes/width/' + props.attrId 
			const data = { value: props.width }
			
			axios.put(url, data)
		}

		document.addEventListener('mouseup', handleMouseUp, true)

		isResizing &&
		document.addEventListener('mousemove', handleMouseMove, true)
		
	}, [isResizing, props, initialPagX, initialRowWidth])

	return (
        <Resize
            className="hover-bg-p"
            onMouseDown={event => handleMouseDown(event)}
        />
	)
}

export default AttributeItem