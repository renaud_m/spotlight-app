import React from 'react'
import { MdMoreHoriz } from 'react-icons/md'

import DeleteAttribute from './DeleteAttribute'

import RippleSurface from '@superGlobals/RippleSurface'

import MenuDialog from '@superGlobals/MenuDialog'

const AttributeOptions = (props) => (
    <MenuDialog
        trigger={
            <RippleSurface mode="light" className="md-circle-light">
                <MdMoreHoriz/>
            </RippleSurface>               
        }
        content={<DeleteAttribute id={props.id} listId={props.listId}/>}
        className="ml-auto"
    />
)
export default AttributeOptions