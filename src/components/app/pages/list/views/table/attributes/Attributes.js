import React, { useState, useEffect, useContext } from 'react'
import axios from 'axios'
import { Container } from 'react-smooth-dnd'

import AppContext from '@context/AppContext'

import AddAttributeCol from './addAttribute/AddAttributeCol'
import AttributeItem from './AttributeItem'

const Attributes = (props) => {
	const [attributes, setAttributes] = useState([])
	const context = useContext(AppContext)

	useEffect(() => {
		let url = process.env.REACT_APP_API_URL + '/list/attribute/all/' + props.listId
		axios.get(url).then(response => setAttributes(response.data.data))
	}, [props.listId, context.app.lastApiResponse])

	const move = (from, to) => {
		let url = process.env.REACT_APP_API_URL + '/list/attribute/move/' + from + '/' + to + '/' + props.listId

		axios.put(url).then((response) => context.app.insertApiResponse(response.data.query_name))
	}

	return (
		<Container
			dragHandleSelector=".grab-col"
			orientation="horizontal"

			onDrop={e => move(e.removedIndex, e.addedIndex)}

			render={(ref) => (
				<thead>
					<tr ref={ref}>
						<th className="ph-s">Actions</th>
						{
							attributes.map(data => (
								<AttributeItem
									key={data.id}
									id={data.id}
									name={data.name}
									type={data.type}
									width={data.width}
									listId={props.listId}
								/>
							))
						}
						<AddAttributeCol listId={props.listId} />
					</tr>
				</thead>
			)}
		/>
	)
}

export default Attributes