import React, { useContext } from 'react'
import axios from 'axios'
import { MdDelete } from 'react-icons/md'

import AppContext from '@context/AppContext'

import RippleSurface from '@superGlobals/RippleSurface'

const DeleteAttribute = (props) => {
    const context = useContext(AppContext)

    const handleClick = () => {
        let url = process.env.REACT_APP_API_URL + '/list/attribute/delete/' + props.id + '/' + props.listId

        axios.delete(url).then((response) => context.app.insertApiResponse(response.data.query_name))
    }

    return (
        <RippleSurface mode="dark" className="flex-row justify-start items-center color-black hover-bg-wd p-s" onClick={ handleClick }>
            <MdDelete className="delete mr-s"/>
            <span className="font-m-700">Supprimer</span>
        </RippleSurface>

    )
}

export default DeleteAttribute