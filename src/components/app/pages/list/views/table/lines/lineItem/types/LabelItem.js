import React from 'react'
import styled from 'styled-components'

import { labelsColorPalette } from '@lib/palettes/LabelsColorPalette'
import LabelMenu from '@lib/menus/labelMenu/LabelMenu'
  
const Label = styled.label(props => ({
    backgroundColor: labelsColorPalette[props.color].main,
    color: labelsColorPalette[props.color].contrast,
}))

const LabelItem = (props) => (
    <td data-type="label">
        <Label color={props.color} className="flex-row justify-center items-center position-absolute-tl w-100 h-100 font-m-700">
            <span>{props.value}</span>
        </Label>
        <LabelMenu id={props.id} attrId={props.attrId} trigger={<div className="w-100 h-100 cursor-pointer" />} />
    </td>
)

export default LabelItem