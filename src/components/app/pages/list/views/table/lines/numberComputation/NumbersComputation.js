import React, { useState, useEffect, useContext } from 'react'
import axios from 'axios'
import { MdSettings } from "react-icons/md"

import AppContext from '@context/AppContext'
import ComputationsMenu from './computationsMenu/ComputationsMenu'

import MenuDialog from '@superGlobals/MenuDialog'
import RippleSurface from '@superGlobals/RippleSurface'

const NumberComputation = (props) => {
    const context = useContext(AppContext)

    const [result, setResult] = useState()
    const [computeName, setComputeName] = useState()

    const [menu, setMenu] = useState()

    useEffect(() => {
        const getResultByComputation = (data, type) => {

            if (type === 'sum') {
                let value = data.reduce((a, b) => a + b.value, 0)
                setResult(value + (data[0].unit && ' ' + data[0].unit))
            }

            if (type === 'average') {
                let value = data.reduce((a, b) => a + b.value, 0) / data.length
                setResult(value + (data[0].unit && ' ' + data[0].unit))
            }

            const nameByType = {
                sum: 'Somme',
                average: 'Moyenne',
            }

            setComputeName(nameByType[type])
        }

        const computeCol = () => {
            let url = process.env.REACT_APP_API_URL + '/list/attribute/get/number_values/' + props.attrId

            axios.get(url).then(response => {
                if (response.data.data[1][0] !== undefined) {
                    let data = response.data.data[0]
                    let type = response.data.data[1][0].type
                    let id = response.data.data[1][0].id
                    let unit = response.data.data[0][0].unit

                    getResultByComputation(data, type)
                    
                    setMenu(
                        <MenuDialog
                            trigger={
                                <RippleSurface mode="dark" className="md-circle-dark cursor-pointer">
                                    <MdSettings />
                                </RippleSurface>
                            }

                            content={
                                <ComputationsMenu
                                    attrId={props.attrId}
                                    computeId={id}
                                    type={type}
                                    unit={unit}
                                />
                            }

                            title="Calcul"
                            headline
                            className="position-absolute-vr mh-s"
                            contentClassName="p-m"
                        />
                    )
                }
            })
        }

        computeCol()

    }, [context.app.lastApiResponse, props.attrId])

    return (
        <td data-type="computation">
            <div className="flex-column items-center w-100 h-100 pv-s">
                <span className="font-m-700">{result}</span>
                <span className="font-s-400">{computeName}</span>
            </div>   
            {menu}
        </td>
    )
}

export default NumberComputation