import React from 'react'

import DeleteLine from './DeleteLine'
import ViewLine from './ViewLine'

const LineOptions = (props) => (
    <div>
        <DeleteLine line={props.line} listId={props.listId} />
        <ViewLine line={props.line} listId={props.listId} />
    </div>
)

export default LineOptions