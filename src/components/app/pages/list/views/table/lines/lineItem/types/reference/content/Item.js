import React from 'react'
import axios from 'axios'

import { MdClear } from 'react-icons/md'
import styled from 'styled-components'

import RippleSurface from '@superGlobals/RippleSurface'
import LineView from '@lib/modals/LineView'

const StyledItem = styled.div`
    background-color: #e7e7e7;
    color: #000;
    transition: background-color .3s ease;

    &:hover { background-color: #d7d7d7 }
`

const IconContainer = styled.div`
    pointer-events: all !important;

    svg { font-size: 18px }
`

const Item = (props) => {
    const handleClick = (event, line) => {
        event.stopPropagation()

        props.removeItem(props.index, props.items)

        let url = process.env.REACT_APP_API_URL + '/list/' + props.listId + '/reference/delete/' + props.id

        axios.delete(url, {
                data: {
                    currentLine: props.currentLine,
                    pivotListId: props.pivotListId,
                    referenceLine: line
                }
            }
        )
    }

    return (
        <LineView
            trigger={
                <RippleSurface mode="dark">
                    <StyledItem className="rounded pv-t ph-s cursor-pointer flex-row justify-center">
                        <span className="font-s">{props.data.value}</span>
                        <IconContainer className="ml-t hover-alpha-5 flex-row justify-center" onClick={event => handleClick(event, props.data.line)}>
                            <MdClear />
                        </IconContainer>
                    </StyledItem>
                </RippleSurface>
            }

            line={props.referenceLine}
            listId={props.pivotListId}
        />

    )
}
	
export default Item