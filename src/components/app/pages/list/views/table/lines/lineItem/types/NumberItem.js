import React from 'react'

import EditableField from '@superGlobals/EditableField'
import StylePicker from '../../global/StylePicker'
import { colorPalette, getCustomColorFromProps } from '../../global/ColorPalette'

import styled from 'styled-components'

const Value = styled.span(props => ({ color: props.color }))

const NumberItem = (props) => (
	<td data-type="number">
		<div className="flex-row">
			<EditableField
				render={<Value className="ph-m" color={getCustomColorFromProps(props.color)} />}
				value={props.value}
				after={props.unit}
				mode="dark"
				dataTable="number_values"
				datafield="value"
				dataId={props.id}/>
		</div>

		<StylePicker table="number_values" color={props.color} palette={colorPalette} itemId={props.id} />
	</td>
)

export default NumberItem