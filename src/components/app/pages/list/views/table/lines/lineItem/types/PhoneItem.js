import React from 'react'

import EditableField from '@superGlobals/EditableField'

const PhoneItem = (props) => (
    <td data-type="phone">
        <div className="flex-row w-100 h-100">
            <EditableField
                render={<a href={'tel:' + props.value}>_</a>}
                textfield="input"
                value={props.value}
                dataTable="phone_values"
                datafield="value"
                className="flex-row justify-center items-center"
                dataId={props.id}
            />
        </div>
    </td>
)

export default PhoneItem