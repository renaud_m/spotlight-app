import React from 'react'

import ComputationTypes from './ComputationTypes'
import ComputationUnits from './ComputationUnits'

const ComputationMenu = (props) => (
    <>
        <div className="mb-m flex-column items-start">
             <div className="font-m-700 mb-s">Type</div>
             <div className="w-100">
                <ComputationTypes
                    computeId={props.computeId}
                    type={props.computeType}
                    attrId={props.attrId}
                />
            </div>
        </div>

        <div className="flex-column items-start">
            <div className="font-m-700 mb-s">Unité</div>
            <ComputationUnits
                attrId={props.attrId}
                computeId={props.computeId}
                type={props.computeType}
                unit={props.unit}
            />
        </div>
    </>
)

export default ComputationMenu