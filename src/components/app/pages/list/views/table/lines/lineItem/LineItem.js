import React from 'react'

import TextItem from './types/TextItem'
import LabelItem from './types/LabelItem'
import NumberItem from './types/NumberItem'
import DateItem from './types/DateItem'
import ReferenceItem from './types/reference/ReferenceItem'
import PhoneItem from './types/PhoneItem'
import LinkItem from './types/LinkItem'
import EmailItem from './types/EmailItem'
import PercentageItem from './types/PercentageItem'

const LineItem = (props) => {
	const getComponentByType = (type) => {
		const components = {
			label: (
				<LabelItem
					value={props.value}
					color={props.stack.color}
					id={props.id}
					attrId={props.attrId}
				/>
			),
			text: (
				<TextItem
					value={props.value}
					color={props.stack.color}
					important={props.stack.important}
					id={props.id}
				/>
			),
			number: (
				<NumberItem
					value={props.value}
					unit={props.stack.unit}
					color={props.stack.color}
					important={props.stack.important}
					id={props.id}
				/>
			),
			date: (
				<DateItem
					value={props.value}
					id={props.id}
				/>
			),
			reference: (
				<ReferenceItem
					value={props.value}
					id={props.id}
					listId={props.listId}
					attrId={props.attrId}
					currentLine={props.line}
				/>
			),
			phone: (
				<PhoneItem
					value={props.value}
					id={props.id}
					listId={props.listId}
				/>
			),
			link: (
				<LinkItem
					value={props.value}
					id={props.id}
					listId={props.listId}
				/>
			),
			email: (
				<EmailItem
					value={props.value}
					id={props.id}
					listId={props.listId}
				/>
			),
			percentage: (
				<PercentageItem
					value={props.value}
					id={props.id}
					listId={props.listId}
				/>
			)
		}

		return components[type]
	}

	return getComponentByType(props.type)
}

export default LineItem