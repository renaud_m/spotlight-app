import React from 'react'
import styled from 'styled-components'

import TextField from '@superGlobals/TextField'

const StickyContainer = styled.div`
    position: sticky;
    top: -20px;
    left: 0;
    z-index: 100;
    display: flex;
    align-items: flex-end;
`
const SearchReference = (props) => {
    const handleChange = (value) => {
        const results = props.buffer.filter(
            reference => reference.length > 0 && reference[0].value.toLowerCase().includes(value.toLowerCase())
        )

        props.setItems(results)
    }

    return (
        <StickyContainer>
            <TextField
                className="w-100 pv-m bg-white"
                label="Rechercher dans la sélection"
                onChange={event => handleChange(event.target.value)} 
            />
        </StickyContainer>
    )
}
	
export default SearchReference