import React, { useContext } from 'react'
import axios from 'axios'

import RippleSurface from '@superGlobals/RippleSurface'
import AppContext from '@context/AppContext'

import ReferenceLine from '@lib/types/reference/ReferenceLine'

const ReferencePicker = (props) => {
    let context = useContext(AppContext)

    const handleCLick = (data) => {
        props.setSelectedItem(data)

        let url = process.env.REACT_APP_API_URL + '/list/' + props.listId + '/reference/addItem'
        
        axios.post(url, {
            referenceValueId: props.id,
            referenceLine: data.line,
            currentLine: props.currentLine,
            pivotListId: props.pivotListId
        }).then(() => context.app.setModal(false))
    }

    return (
        <RippleSurface mode="dark" className="rounded-m hover-bg-wd" onClick={() => handleCLick(props.data[0])}>
            <ReferenceLine
                data={props.data} 
                attrId={props.attrId}
                listId={props.listId}
                id={props.id}
            />
        </RippleSurface>
    )
}
	
export default ReferencePicker