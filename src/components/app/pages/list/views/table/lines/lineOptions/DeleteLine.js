import React, { useContext } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import { MdDelete } from 'react-icons/md'

import Appcontext from '@context/AppContext'
import RippleSurface from '@superGlobals/RippleSurface'

const DeleteLine = (props) => {
    const params = useParams()
    const context = useContext(Appcontext)

    const handleClick = () => {
        let url = process.env.REACT_APP_API_URL + '/list/line/delete/' + props.line + '/' + params.listId
		axios.delete(url).then(response =>  context.app.insertApiResponse(response.data.query_name))
    }
    
    return (
        <RippleSurface mode="dark" transfert>
            <div className="flex-row justify-start items-center p-s hover-bg-wd" onClick={handleClick}>
                <MdDelete className="mr-s"/>
                <span className="font-700">Supprimer</span>
            </div>
        </RippleSurface>
    )
}

export default DeleteLine