import React, {useContext} from 'react'
import axios from 'axios'

import AppContext from '@context/AppContext'

import Button from '@superGlobals/Button'


const AddLineButton = (props) =>  {
	const context = useContext(AppContext)

	const handleClick = () => {
		let url = process.env.REACT_APP_API_URL + '/list/line/add/' + props.listId
		axios.post(url).then(response => context.app.insertApiResponse(response.data.query_name))
	}

	return (
		<Button
			importance="primary"
			type="button"
			text="Ajouter une ligne"
			onClick={handleClick}
			className="mt-m"
		/>
	)
}
	

export default AddLineButton