import React, { useState } from 'react'

import SearchReference from './SearchReference'
import ReferenceList from './ReferenceList'

const ModalContent = (props) => {
    let [items, setItems] = useState([])
    let [buffer, setBuffer] = useState([])

    return (
        <>
            <SearchReference
                buffer={buffer}
                setItems={setItems}
            />

            <ReferenceList
                id={props.id}
                listId={props.listId}
                attrId={props.attrId}
                currentLine={props.currentLine}
                setSelectedItem={props.setSelectedItem}
                items={items}
                setItems={setItems}
                setBuffer={setBuffer}
            />
        </>
    )
}
	
export default ModalContent