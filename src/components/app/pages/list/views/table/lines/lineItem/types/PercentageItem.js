import React from 'react'

import styled from 'styled-components'

import EditableField from '@superGlobals/EditableField'

const Percentage = styled.span`
	z-index: 0;
	font-weight: ${props => (props.width === 100) ? 700 : 400};
	color: ${props => (props.width === 100) ? `#fff` : `#000`};

	&:after {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		background-color: #41DFA4;
		height: 100%;
		width: ${props => props.width + '%'};
		z-index: -1;
		transition: width .3s ease;
	}

`

const PercentageItem = (props) => (
	<td data-type="percentage">
		<div className="flex-row">
			<EditableField
				render={<Percentage width={props.value} className="ph-m" />}
				value={props.value}
				after="%"
				mode="dark"
				dataTable="percentage_values"
				datafield="value"
				dataId={props.id}/>
		</div>
	</td>
)

export default PercentageItem