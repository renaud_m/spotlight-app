import React, { useState, useEffect } from 'react'
import axios from 'axios'

import ReferencePicker from './ReferencePicker'

import Spinner from '@superGlobals/Spinner'

const ReferenceList = (props) => {
    const [content, setContent] = useState(<Spinner size="64" color="black" />)
    const [references, setReferences] = useState([])
    const [pivotListId, setPivotListId] = useState([])
    const [data, setData] = useState('')

    useEffect(() => {
        let apiUrl = process.env.REACT_APP_API_URL

        axios.get(apiUrl + '/list/attribute/getReference/' +  props.attrId + '/' + props.id).then(response => {
            let pivotListId = response.data.data.attributeReference.pivot_list_id
            setPivotListId(pivotListId)

            let params = { params: { exeptions: response.data.data.selectedItems } }

            axios.get(apiUrl + '/list/value/all/' +  pivotListId, params).then(response => {
                (response.data.data.length === 0) ?
                setContent(<div className="mbox-bm">Aucune référence disponible pour cette valeur</div>)
                :
                setData(response.data.data)
            })
        })
    }, [props.attrId, props.id])

    useEffect(() => {
        props.setItems(data)
        props.setBuffer(data)
    }, [data])

    useEffect(() => {
        let references = []

        for (let i = 0; i < props.items.length; i++) {
            let item = props.items[i]

            if (item.length > 0) {
                references.push(
                    <ReferencePicker
                        key={i}
                        id={props.id}
                        data={item}
                        listId={props.listId}
                        attrId={props.attrId}
                        pivotListId={pivotListId}
                        currentLine={props.currentLine}
                        setSelectedItem={props.setSelectedItem}
                    />
                )
            }
        }

        setReferences(references)
    }, [props, pivotListId])
    

    useEffect(() => {
        setContent(<div className="mbox-bm">{references}</div>)
    }, [references])

    return content
}
	
export default ReferenceList