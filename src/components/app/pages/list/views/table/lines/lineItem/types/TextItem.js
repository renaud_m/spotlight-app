import React from 'react'

import EditableField from '@superGlobals/EditableField'

import { colorPalette, getCustomColorFromProps } from '../../global/ColorPalette'
import StylePicker from '../../global/StylePicker'

import styled from 'styled-components'

const Value = styled.span(props => ({ color: props.color }))

const TextItem = (props) => (
	<td data-type="text">
		<EditableField
			render={<Value className="ph-m" color={getCustomColorFromProps(props.color)} />}
			value={props.value}
			mode="dark"
			dataTable="text_values"
			datafield="value"
			dataId={props.id}/>

		<StylePicker table="text_values" color={props.color} palette={colorPalette} itemId={props.id} />
	</td>
)

export default TextItem