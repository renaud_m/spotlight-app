import React, { useContext } from 'react'
import axios from 'axios'

import AppContext from '@context/AppContext'
import Select from '@superGlobals/Select'

const ComputationUnits = (props) => {
    const context = useContext(AppContext)

    const items = [
        {
            value: '',
            text: 'aucune'
        },
        {
            value: 'Devises',
            text: [
                {value: '€', text:'€'},
                {value: '$', text:'$'},
                {value: '£', text:'£'}
            ],
        },
        {
            value: 'Temporelles',
            text: [
                {value: 'j', text:'j'},
                {value: 'h', text:'h'},
                {value: 'm', text:'m'}
            ],
        },
        {
            value: 'Masse',
            text: [
                {value: 'kg', text:'kg'},
                {value: 'g', text:'g'}
            ],
        },
        {
            value: 'Mesures',
            text: [
                {value: 'm', text:'m'},
                {value: 'cm', text:'cm'},
                {value: 'mm', text:'mm'}
            ],
        }
    ]

    const getSelected = () => {
        if (props.unit === '' || props.unit === null) {
            return items[0]
        } else {
            for (let i = 0; i < items.length; i++) {
                const item = items[i]

                if (typeof item.text !== 'string') {
                    for (let i = 0; i < item.text.length; i++) {
                        if (item.text[i].value === props.unit) return item.text[i]
                    }
                }
            }
        }
    }

    const handleChange = (unit) => {
        let url = process.env.REACT_APP_API_URL + '/list/attribute/edit/' + props.attrId + '/number_values/unit'
        axios.put(url, { value: unit } ).then((response) => context.app.insertApiResponse(response.data.query_name))
    }
    
    return (
        <Select items={items} selected={getSelected()} maxHeight="150" onChange={unit => handleChange(unit)} />
    )
}

export default ComputationUnits