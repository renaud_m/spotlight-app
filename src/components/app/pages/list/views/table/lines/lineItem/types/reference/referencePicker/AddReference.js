import React, { useContext } from 'react'
import { MdAdd } from 'react-icons/md'

import AppContext from '@context/AppContext'

import RippleSurface from '@superGlobals/RippleSurface'

import ModalContent from './ModalContent'

const AddReference = (props) => {
    let context = useContext(AppContext)

    const handleClick = (items) => {
        context.app.setModal(true, 'Choisissez votre référence',
            <ModalContent
                id={props.id}
                listId={props.listId}
                attrId={props.attrId}
                currentLine={props.currentLine}
                setSelectedItem={props.setSelectedItem}
            />
        )
    }


    return (
        <RippleSurface
            mode="dark"
            className="md-circle-dark position-absolute-vr mh-s cursor-pointer"
            onClick={handleClick}
            children={<MdAdd />}
        />
    )

}
	
export default AddReference