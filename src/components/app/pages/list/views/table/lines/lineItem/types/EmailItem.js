import React from 'react'

import EditableField from '@superGlobals/EditableField'

const EmailItem = (props) => (
    <td data-type="email">
        <div className="flex-row w-100 h-100">
            <EditableField
                render={<a href={'mailto:' + props.value}>_</a>}
                textfield="input"
                value={props.value}
                dataTable="email_values"
                datafield="value"
                dataId={props.id}
                className="flex-row justify-center items-center"
            />
        </div>
    </td>
)

export default EmailItem