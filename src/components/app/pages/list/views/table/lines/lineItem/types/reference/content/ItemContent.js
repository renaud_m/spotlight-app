import React, { useState, useEffect } from 'react'
import axios from 'axios'

import Item from './Item'

const ItemContent = (props) => {
    const [items, setItems] = useState([])

    useEffect(() => {
        let apiUrl = process.env.REACT_APP_API_URL

        axios.get(apiUrl + '/list/' + props.listId + '/reference/all/' + props.attrId + '/' + props.id).then(response => {
            if (response.data.data.attributeReference !== null) {
                let pivotListId = response.data.data.attributeReference.pivot_list_id
                let referenceValueItems = response.data.data.referenceValueItem

                for (let i = 0; i < referenceValueItems.length; i++) {
                    let id = referenceValueItems[i].id
                    let referenceLine = referenceValueItems[i].reference_line

                    axios.get(apiUrl + '/list/line/get/' + referenceLine + '/' + pivotListId).then(response => {
                        let item = {
                            data: response.data.data[0],
                            params: {
                                id: id,
                                pivotListId: pivotListId,
                                referenceLine: referenceLine
                            }
                        }

                        if (props.selectedItem) {

                            let selectedItem = {
                                data: props.selectedItem,
                                params: {
                                    id: id,
                                    pivotListId: pivotListId,
                                    referenceLine: referenceLine
                                }
                            }

                            if (props.selectedItem.id === item.data.id) setItems(items => [...items, selectedItem])
                        } else setItems(items => [...items, item])
                    })
                }
            }
        })
    }, [props.attrId, props.selectedItem, props.listId, props.id, props.currentLine])


    const removeItem = (index, items) => {
        items.splice(index, 1)
        let result = items.filter(value => value)

        setItems(result)
    }

    return (
        <div>
            <div className="flex-row mbox-rs mh-m">
                {
                    items.map((item, index) => (
                            <Item
                                key={index}
                                index={index}
                                data={item.data}
                                id={item.params.id}
                                listId={props.listId}
                                pivotListId={item.params.pivotListId}
                                currentLine={props.currentLine}
                                items={items}
                                removeItem={removeItem}
                                referenceLine={item.params.referenceLine}
                            />
                        )
                    )
                }
            </div>
        </div>
    )

}
	
export default ItemContent