import React from 'react'

import EditableField from '@superGlobals/EditableField'

const LinkItem = (props) => (
    <td data-type="link">
        <div className="flex-row w-100 h-100">
            <EditableField
                render={<a href={props.value} target="_blank" rel="noopener noreferrer">_</a>}
                textfield="input"
                value={props.value}
                dataTable="link_values"
                datafield="value"
                className="flex-row justify-center items-center"
                dataId={props.id}
            />
        </div>
    </td>
)

export default LinkItem