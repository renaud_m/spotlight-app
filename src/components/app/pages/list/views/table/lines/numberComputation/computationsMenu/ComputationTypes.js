import React, { useContext, useEffect, useState } from 'react'
import axios from 'axios'

import AppContext from '@context/AppContext'
import RippleSurface from '@superGlobals/RippleSurface'

const ComputationTypes = (props) => {
    const context = useContext(AppContext)
    const [selectedComputation, setSelectedComputation] = useState()

    const computations = [
        {
            name: 'sum',
            symbol: 'Somme'
        },
        {
            name: 'average',
            symbol: 'Moyenne'
        }
    ]

    const handleClick = (computation) => {
        let url = process.env.REACT_APP_API_URL + '/list/value/edit/attribute_computations/type/' + props.computeId

        axios.put(url, { value: computation }).then((response) => {
            setSelectedComputation(computation)
            context.app.insertApiResponse(response.data.query_name)
        })
    }

    useEffect(() => {
        let url = process.env.REACT_APP_API_URL + '/list/attribute/getComputation/' + props.attrId
        axios.get(url).then(response => setSelectedComputation(response.data.data.type))
    }, [props.attrId])

    return (
        <div className="flex-row">
        {
            computations.map((computation, index) => (
                <RippleSurface
                    key={index}
                    mode={(selectedComputation === computation.name) ? 'dark' : 'light'}
                    onClick={() => handleClick(computation.name)}
                    className={(selectedComputation === computation.name ? 'bg-primary color-primary-c font-m-700' : 'bg-secondary color-secondary-c') + ' ph-m pv-t col-extend font-m-400'}
                    children={computation.symbol}
                />
            ))
        }
        </div>
    )
}

export default ComputationTypes