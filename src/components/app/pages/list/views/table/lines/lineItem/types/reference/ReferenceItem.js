import React, { useState } from 'react'

import AddReference from './referencePicker/AddReference'
import ItemContent from './content/ItemContent'

const ReferenceItem = (props) => {
    const [selectedItem, setSelectedItem] = useState()

    return (
        <td data-type="text">
            <ItemContent
                attrId={props.attrId}
                listId={props.listId}
                id={props.id}
                currentLine={props.currentLine}
                selectedItem={selectedItem}
            />

            <AddReference
                setSelectedItem={setSelectedItem}
                currentLine={props.currentLine}
                listId={props.listId}
                attrId={props.attrId}
                id={props.id}
            />
        </td>
    )

}
	
export default ReferenceItem