import React, { useEffect, useContext, useState } from 'react'
import axios from 'axios'
import { Container, Draggable } from 'react-smooth-dnd'
import { MdGrain } from 'react-icons/md'

import LineItem from './lineItem/LineItem'
import LineMenu from './LineMenu'
import AppContext from '@context/AppContext'
import NumberComputation from './numberComputation/NumbersComputation'

const Lines = (props) => {
	const context = useContext(AppContext)
	const [lines, setLines] = useState([])
	
	useEffect(() => {
		const getLines = (lines) => {
			let content = []
			let hasNumberValue = []
	
			const stacks = {
				text: ['color', 'important'],
				number: ['color', 'important', 'unit'],
				label: ['color']
			}
	
			const generateStack = (type, data) => {
				let result = {}
	
				for (let stack in stacks) {
					if (type === stack) {
						let values = stacks[stack]
	
						for (let value of values) {
							result = Object.assign({}, {
								[value]: data[value]
							}, result)
						}
					}
				}
	
				return result
			}
			
			for (let i = 0; i < lines.length; i++) {
				let line = lines[i]
				let lineIndex = i + 1
	
				let item = line.map((data) => (
					<LineItem
						key={data.id + data.attribute_id}
						id={data.id}
						type={data.attribute_type}
						value={data.value}
						attrId={data.attribute_id}
						listId={props.listId}
						line={lineIndex}
						stack={generateStack(data.attribute_type, data)} />
				))
	
				let result = (
					<Draggable key={lineIndex} render={() => (
						<tr>
							<td>
								<i className="grab-line" onMouseDown={handleMouseDown}><MdGrain /></i>
								<LineMenu line={lineIndex} listId={props.listId}/>
							</td>
							{item}
							<td></td>
						</tr>
					)} />
				)
				
				content.push(result)
			}

			for (let data of lines[0]) {
				if (data.attribute_type === 'number') {
					hasNumberValue.push({ attrId: data.attribute_id, position: data.column })
				}
			}
			
			if (hasNumberValue !== '') {
				let rows = []
				
				for (let i = 0; i < lines[0].length; i++) rows.push(<td key={i}></td>)
	
				hasNumberValue.map(data => rows.splice(data.position - 1, 1, <NumberComputation key={data.attrId} attrId={data.attrId}/>))

				content.push(
					<tr key={0}>
						<td></td>
						{rows}
						<td></td>
					</tr>
				)
			}
	
			return content
		}

		const getListLines = () => {
			let url = process.env.REACT_APP_API_URL + '/list/value/all/' + props.listId

			axios.get(url).then(response => {
				(response.data.data.length) && setLines(getLines(response.data.data))
			})
		}

		getListLines()

	}, [context.app.lastApiResponse, props.listId])

	const move = (from, to) => {
		let url = process.env.REACT_APP_API_URL + '/list/line/move/' + from + '/' + to + '/' + props.listId
		axios.put(url).then((response) => context.app.insertApiResponse(response.data.query_name))
	}

	const handleMouseDown = (event) => {
		let parent = event.target.closest('tr')
		let cells = parent.querySelectorAll('td:not(:first-child)')

		for (let i = 0; i < cells.length; i++) {
			let cell = cells[i]
			let cellWidth = cell.clientWidth

			cell.style.width = cellWidth + 'px'
		}
	}

	useEffect(() => {
	}, [lines])

	return (
		<Container
			dragClass="is-dragged"
			dragHandleSelector=".grab-line"
			onDrop={e => move(e.removedIndex + 1, e.addedIndex + 1)}
			render={(ref) => (
			<>
				<tbody ref={ref}>
					{lines}
				</tbody>
			</>
		)} />
	)
}

export default Lines