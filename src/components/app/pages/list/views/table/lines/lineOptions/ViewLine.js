import React from 'react'
import { useParams } from 'react-router-dom'
import { MdRemoveRedEye } from 'react-icons/md'
import RippleSurface from '@superGlobals/RippleSurface'

import LineView from '@lib/modals/LineView'

const ViewLine = (props) => {
    const params = useParams()

    return (
        <LineView
            trigger={
                <RippleSurface mode="dark" className="flex-row justify-start items-center p-s hover-bg-wd">
                    <MdRemoveRedEye className="mr-s"/>
                    <span className="font-700">Voir</span>
                </RippleSurface>
            }
            line={props.line}
            listId={params.listId}
        />
    )
}

export default ViewLine