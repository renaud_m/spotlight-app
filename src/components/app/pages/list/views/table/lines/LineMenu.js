import React from 'react'
import { MdMoreHoriz } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'

import LineOptions from './lineOptions/LineOptions'

import MenuDialog from '@superGlobals/MenuDialog'

const LineMenu = (props) => (
    <MenuDialog
        trigger={
            <RippleSurface mode="dark" className="md-circle-dark">
                <MdMoreHoriz />
            </RippleSurface>
        }
        content={<LineOptions line={props.line} listId={props.listId} />}
        className="flex-row justify-center"
    />
)

export default LineMenu