import React from 'react'

import { MdTextFormat } from 'react-icons/md'

import MenuDialog from '@superGlobals/MenuDialog'
import RippleSurface from '@superGlobals/RippleSurface'

import StyleMenu from './StyleMenu'

const ItemOptions = (props) => (
    <MenuDialog
        trigger={
            <RippleSurface mode="dark"
                className="w-auto h-auto md-circle-dark"
                children={<MdTextFormat />}
            />
        }
        content={<StyleMenu table={props.table} itemId={props.itemId} color={props.color} />}
        className="position-absolute-vr mh-s"
        contentClassName="p-m"
        title="Style"
    />
)

export default ItemOptions