import React, { useContext, useState } from 'react'
import axios from 'axios'

import { MdCheck } from "react-icons/md";

import styled from 'styled-components'
import AppContext from '@context/AppContext'

import { colorPalette } from './ColorPalette'

const ColorPicker = styled.span(props => ({
    height: '20px',
    width: '20px',
    backgroundColor: props.color,
    color: props.contrast,
    borderRadius: '50%',
    cursor: props.cursor
}))

const StyleMenu = (props) => {
    const context = useContext(AppContext)
    const [selectedColor, setSelectedColor] = useState(props.color)

    const edit = (target, value) => {
        setSelectedColor(value)

        let url = process.env.REACT_APP_API_URL + '/list/value/edit/' + props.table + '/' + target + '/' + props.itemId + '/?value=' + value
        
        axios.put(url).then((response) => context.app.insertApiResponse(response.data.query_name))
    } 

    const getColorPickers = () => {
        let result = []

        for (let color in colorPalette) {
            result.push(
                <ColorPicker
                    key={colorPalette[color].id}
                    cursor={ (color === selectedColor) ? 'default' : 'pointer' }
                    color={colorPalette[color].main}
                    contrast={colorPalette[color].contrast}
                    className="flex-row justify-center items-center"
                    onClick={() => edit('color', color)}>

                    { (color === selectedColor) && <MdCheck className="font-m" /> } 
                </ColorPicker>
            )
        }

        return result
    }
   
	return (
        <div>
            <div className="font-m-700 mb-s align-left">Couleurs</div>
            <div className="mbox-rs flex">
                { getColorPickers() }
            </div>
        </div>
	)
}

export default StyleMenu