import React, { useState } from 'react'
import axios from 'axios'

import DatePicker from 'react-datepicker'

import "react-datepicker/dist/react-datepicker.css";

const DateItem = (props) => {
	const [date, setDate] = useState(new Date(props.value))

	const convert = (dateString) => {
		const date = new Date(dateString)
		let m = ("0" + (date.getMonth() + 1)).slice(-2)
		let d = ("0" + date.getDate()).slice(-2)

		return [date.getFullYear(), m, d].join("-")
	}

	const handleChange = (date) => {
		let url = process.env.REACT_APP_API_URL + '/list/edit/date_values/value/' + props.id
		axios.put(url, { value: convert(date) }).then(() => { setDate(date) })
	}
	
	return (
		<td data-type="date">
			<div className="w-100 h-100 flex-row justify-center position-relative hover-bg-wd">

				<DatePicker
					selected={date}
					onChange={date => handleChange(date)}
					dateFormat="dd/MM/yyyy"
					showWeekNumbers
				/>
			</div>
		</td>
	)
}

export default DateItem