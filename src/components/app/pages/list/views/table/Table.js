import React, { useEffect, useState } from 'react'

import AddLineButton from './lines/AddLineButton'

import Attributes from './attributes/Attributes'
import Lines from './lines/Lines'

const Table = (props) => {
	const [tableContent, setTableContent] = useState()
	
	useEffect(() => {
		setTableContent(
			<>
				<Attributes listId={props.listId} />
				<Lines listId={props.listId} />
			</>
		)
	}, [props.listId])

	return (
		<div className="h-100">
			<div>
				<table className="h-100 w-100 mb-m">
					{tableContent}
				</table>
			</div>


			<AddLineButton listId={props.listId}/>
		</div>
	)	
	
}

export default Table