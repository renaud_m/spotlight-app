import React, { useEffect, useState } from 'react'
import axios from 'axios'

import { useParams } from 'react-router-dom'

import Card from './Card'

const Cards = (props) => {
    const [cards, setCards] = useState([])
    const params = useParams()

    useEffect(() => {
        let url = process.env.REACT_APP_API_URL + '/list/value/all/' + params.listId

        axios.get(url).then(response => {
			(response.data.data.length) && setCards(response.data.data)
		})
    }, [params.listId])

    const getCards = () => (
        cards.map((card, index) => (
            <Card data={card} key={index} />
        ))
    )

	return (
		<div className="grid-m h-100">
            {getCards()}
		</div>
	)	
}

export default Cards