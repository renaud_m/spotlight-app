import React from 'react'
import styled from 'styled-components'

import { useParams } from 'react-router-dom'

import LineView from '@lib/modals/LineView'
import TypedIcon from '@lib/utils/TypedIcon'
import TypedValue from '@lib/utils/TypedValue'
import RippleSurface from '@superGlobals/RippleSurface'

const StyledCard = styled.article`
    transition: all .3s ease;

    &:hover {
        transform: translateY(-5px);
        box-shadow: 0 5px 10px rgba(0, 0, 0, .1);
    }
`

const Card = (props) => {
    const params = useParams()

    return (
        <div className="col-12 xl:col-3 l:col-4 m:col-6">
            <StyledCard>
                <LineView
                    trigger={
                        <RippleSurface mode="dark" className="rounded">
                            <article className="rounded bg-white mbox-bm p-m h-100 cursor-pointer">
                            {
                                props.data.map((data, index) => (
                                    <div key={index}>
                                        <div className="flex-row justify-start items-center mb-t">
                                            <TypedIcon type={data.attribute_type} className="font-m" />
                                            <h2 className="font-m-700 ml-s">{data.column_name}</h2>
                                        </div>
                                        <TypedValue type={data.attribute_type} data={data} />
                                    </div>
                                ))
                            }
                            </article>
                        </RippleSurface>
                    }
                    line={props.data[0].line}
                    listId={params.listId}
                />
            </StyledCard>
        </div>
    )
}

export default Card