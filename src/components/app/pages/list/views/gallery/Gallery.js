import React from 'react'

import Cards from './cards/Cards'

const Gallery = (props) => {
	return (
		<div className="h-100">
            <Cards />
		</div>
	)	
}

export default Gallery