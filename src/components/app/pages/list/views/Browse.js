import React, { useEffect, useState } from 'react'

import Table from './table/Table'
import Gallery from './gallery/Gallery'

const Browse = (props) => {
	const [view, setView] = useState()
	
	useEffect(() => {
        let views = {
            table: <Table listId={props.listId} />,
            gallery: <Gallery listId={props.listId} />
        }
    
        setView(views[props.view])

	}, [props.listId, props.view])

    return <>{view}</>
}

export default Browse