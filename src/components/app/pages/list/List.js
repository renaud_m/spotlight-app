import React, { useEffect, useState, useContext } from 'react'
import axios from 'axios'

import { useHistory } from 'react-router-dom'

import Headline from './headline/Headline'
import Browse from './views/Browse'

import Button from '@superGlobals/Button'
import AppContext from '@context/AppContext'

const List = (props) => {
    let history = useHistory()
    const context = useContext(AppContext)
    const [list, setList] = useState([])
	
	useEffect(() => {
        const url = process.env.REACT_APP_API_URL + '/list/get/' + props.listId
        axios.get(url).then(response => setList(response.data.data))
    }, [props.listId, history, context.app.lastApiResponse])

    return (list === null) ? (
        <div className="flex-column items-center">
            <h1 className="text-uppercase font-l-700">La liste que vous demandez est introuvable</h1>
            <h2 className="font-m-400 mb-m">Êtes-vous sûr(e) que celle-ci existe ?</h2>
            <Button
                importance="primary"
                text="Retour à la sélection"
                onClick={() => history.push('/list/all')}
            />
        </div>
    ) : (
        <>
            <Headline listId={props.listId} />
            <Browse listId={props.listId} view={list.view} />
        </>
    )
}

export default List