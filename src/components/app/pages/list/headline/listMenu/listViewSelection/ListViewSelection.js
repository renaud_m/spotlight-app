import React, { useContext } from 'react'
import axios from 'axios'

import { MdViewList, MdViewModule } from 'react-icons/md'

import Select from '@superGlobals/Select'
import AppContext from '@context/AppContext'

const ListViewSelection = (props) => {
    const context = useContext(AppContext)

    const items = [
        {
            text: (
                <div className="flex-row justify-start items-center">
                    <MdViewList className="mr-s" />
                    <span className="font-700">Table</span>
                </div>
            ),
            value: 'table'
        },
        {
            text: (
                <div className="flex-row justify-start items-center">
                    <MdViewModule className="mr-s" />
                    <span className="font-700">Gallerie</span>
                </div>
            ),
            value: 'gallery'
        },
    ]

    const getSelectedItem = () => {
        for (let i = 0; i < items.length; i++) {
            if (items[i].value === props.view) return items[i]
        }
    }

    const handleChange = (view) => {
        let url = process.env.REACT_APP_API_URL + '/list/value/edit/lists/view/' + props.listId
        
        axios.put(url, { value: view }).then((response) => context.app.insertApiResponse(response.data.query_name))
    }

    return <Select items={items} selected={getSelectedItem()} onChange={view => handleChange(view)} />
}

export default ListViewSelection