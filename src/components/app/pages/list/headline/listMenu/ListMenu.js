import React from 'react'

import ListDownLoad from './listDownload/ListDownload'
import ListViewSelection from './listViewSelection/ListViewSelection'

const ListMenu = (props) => (
    <div className="flex-row justify-start items-center mb-m mbox-rm">
        <ListDownLoad listId={props.listId} />
        <ListViewSelection listId={props.listId} view={props.list.view} />
    </div>
)

export default ListMenu