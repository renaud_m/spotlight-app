import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { CSVLink } from 'react-csv'
import { MdFileDownload } from 'react-icons/md'

import Button from '@superGlobals/Button'

const ListDownload = (props) => {
    const [fileName, setFileName] = useState()
    const [data, setData] = useState([])

    const [isDataComplete, setIsDataComplete] = useState()
 
    useEffect(() => {
        setIsDataComplete(false)
        setData([])

        let apiUrl = process.env.REACT_APP_API_URL

        const getFileName = () => {
            axios.get(apiUrl + '/list/get/' + props.listId).then(response => {
                setFileName(response.data.data.name)
            })
        }

        const getDatas = () => {
            axios.get(apiUrl + '/list/attribute/all/' + props.listId).then((response) => {
                const attributes = response.data.data
                const attributeDatas = []

                for (let i = 0; i < attributes.length; i++) {
                    const attribute = attributes[i]
                    attributeDatas.push(attribute.name)
                }

                setData(data => [...data, attributeDatas])

                axios.get(apiUrl + '/list/value/all/' + props.listId).then((response) => {
                    const lines = response.data.data
                    const lineDatas = []

                    for (let i = 0; i < lines.length; i++) {
                        const line = lines[i]
                        const lineValues = []

                        for (let data of line) {
                            lineValues.push(data.value)
                        }

                        lineDatas.push(lineValues)
                    }
                
                    setData(data => [...data, ...lineDatas])
                    setIsDataComplete(true)
                })
            })
        }

        getFileName()
        getDatas()
    }, [props.listId])

    return (
        <CSVLink
            data={data}
            filename={fileName + '.csv'}
            separator={";"}
            children={
                <Button
                    importance="primary"
                    text="Exporter au format csv"
                    icon={<MdFileDownload />}
                />
            }
            onClick={() => (isDataComplete) ? true : false}
        />
    )
}

export default ListDownload