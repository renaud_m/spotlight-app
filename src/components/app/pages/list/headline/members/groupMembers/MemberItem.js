import React from 'react'

const MemberItem = (props) => (
    <div className="p-t bg-secondary color-secondary-c rounded-full">
        {props.data.firstname[0]}
        {props.data.lastname[0]}
    </div>
)

export default MemberItem