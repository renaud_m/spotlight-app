import React, { useState, useEffect } from 'react'
import axios from 'axios'

import MemberItem from './MemberItem'

const GroupMembers = (props) => {
    const [members, setMembers] = useState([])
    const [membersCount, setMembersCount] = useState(0)

    useEffect(() => {
        const url = 'http://localhost:3333/group/' + props.groupId + '/members'

		axios.get(url).then(response => {
            setMembers(response.data.data.members)
            setMembersCount(response.data.data.membersCount)
        })
	}, [props.groupId])

    return (
        <div className="mt-m w-100">
            <div className="font-m-700 mb-s">Membres ({membersCount})</div>
            <div className="flex-row justify-end mbox-rs">
                {members.map(member => <MemberItem key={member.id} data={member} />)}
            </div>
        </div>
    )
}

export default GroupMembers