import React from 'react'
import { useParams } from 'react-router-dom'

import GroupMembers from './groupMembers/GroupMembers'

const Members = (props) => {
    const params = useParams()

    return (
        <div className="flex-column justify-start position-absolute-tr">
            <GroupMembers groupId={params.groupId} />
        </div>
    )
}

export default Members