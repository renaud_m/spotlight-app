import React, { useContext } from 'react'
import axios from 'axios'

import Button from '@superGlobals/Button'
import AppContext from '@context/AppContext'

const SendInviteButton = (props) => {
    const context = useContext(AppContext)

    const handleClick = (value) => {
        const token = localStorage.getItem('spotlight-token')
		const headers = { headers: { Authorization: `Bearer ${token}` } }

        for (let i = 0; i < props.selection.length; i++) {
            const selection = props.selection[i]

            let data = { type: 'group_invite' }
            
            axios.post('http://localhost:3333/notification/' + selection.id, data, headers)
        }

        context.app.setAlert(true, 'success', 'Invitation(s) envoyée(s)')
    }

    return (
        <Button
            importance="primary"
            text="Envoyer une invitation"
            className="w-100 mt-m"
            onClick={handleClick}
        />
    )
}

export default SendInviteButton