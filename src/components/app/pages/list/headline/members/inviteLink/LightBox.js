
import React from 'react'

import RippleSurface from '@superGlobals/RippleSurface'

const MemberItem = (props) => {

    const handleClick = (result) => {
        props.setSelection(selection => [...selection, result])
        props.setIsSearching(false)
    }

    return (
        <div
            className="position-absolute p-s shadow-m w-100 rounded cursor-pointer index-100 bg-white"
            children={
                props.results.length === 0 ?
                <div>Aucun résultat</div>
                :
                props.results.map(result => (
                        <RippleSurface mode="dark" transfert>
                            <article key={result.id} className="flex-row justify-start items-center hover-bg-wd p-s rounded" onClick={() => handleClick(result)}>
                                <div className="p-s rounded-full flex-row justify-center items-center mr-s">
                                    {result.firstname[0]}
                                    {result.lastname[0]}
                                </div>
                                <div>
                                    <h1 className="font-m-700">{'@' + result.username}</h1>
                                    <h2 className="font-s-400">{result.email}</h2>
                                </div>
                            </article>
                        </RippleSurface>
                    ) 
                )
            }
        />
    )
}

export default MemberItem