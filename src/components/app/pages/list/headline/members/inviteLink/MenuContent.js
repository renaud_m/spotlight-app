import React, { useState, useEffect } from 'react'
import axios from 'axios'

import styled from 'styled-components'

import MenuDialog from '@superGlobals/MenuDialog'
import TextField from '@superGlobals/TextField'

import LightBox from './LightBox'
import SelectItem from './SelectItem'
import SendInviteButton from './SendInviteButton'

const ResultBox = styled.div`
    max-height: 150px;
    height: 100%;
`

const InviteLink = (props) => {
    const [isSearching, setIsSearching] = useState(false)

    const [results, setResults] = useState([])
    const [selection, setSelection] = useState([])

    const handleChange = (value) => {
        
        (value !== '') ? setIsSearching(true) : setIsSearching(false)
        
        let data = {
            params: {
                username: value,
                exeptions: selection
            }
        }

        axios.get('http://localhost:3333/user/search', data).then(response => setResults(response.data.data))
    }

    return (
        <div>
            <div className="position-relative mb-m">
                <TextField
                    label="Nom d'utilisateur"
                    onChange={event => handleChange(event.target.value)}
                />
                {
                    (isSearching)
                    &&
                    <LightBox
                        results={results}
                        selection={selection}
                        setSelection={setSelection}
                        setIsSearching={setIsSearching}
                    />
                }
            </div>
            <ResultBox className="overflow-y-auto mbox-bs">
                {
                    selection.map((item, index) => (
                            <SelectItem
                                key={item.id}
                                index={index}
                                username={item.username} 
                                setSelection={setSelection}
                            />
                        )
                    )
                }
            </ResultBox>
            <SendInviteButton selection={selection} />
        </div>
    )
}

export default InviteLink