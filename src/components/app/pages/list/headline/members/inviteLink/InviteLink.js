import React, { useState, useEffect } from 'react'
import axios from 'axios'

import styled from 'styled-components'

import Button from '@superGlobals/Button'
import MenuDialog from '@superGlobals/MenuDialog'
import TextField from '@superGlobals/TextField'

import MenuContent from './MenuContent'

const InviteLink = (props) => (
    <MenuDialog
        trigger={
            <Button
                text="Inviter"
                className="mt-s"
            />
        }
        content={<MenuContent />}
        title="Inviter sur le groupe"
        contentClassName="p-m"
        width="300"
    />
)

export default InviteLink