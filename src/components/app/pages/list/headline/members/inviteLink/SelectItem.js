import React from 'react'

import { MdClear } from 'react-icons/md'

const SelectItem = (props) => {
    const handleClick = () => {
        props.setSelection(
            selection => (
                [
                    ...selection.slice(0, props.index),
                    ...selection.slice(props.index + 1)
                ]
            )
        )
    }

    return (
        <div className="rounded bg-secondary color-secondary-c flex-row justify-between items-center pv-t ph-s">
            <span className="pr-s">{props.username}</span>
            <MdClear className="cursor-pointer hover-alpha-7" onClick={handleClick} />
        </div>
    )
}

export default SelectItem