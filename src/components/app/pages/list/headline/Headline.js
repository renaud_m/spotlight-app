import React, { useEffect, useState } from 'react'
import axios from 'axios'

import ListInformations from './listInformations/ListInformations'
import ListMenu from './listMenu/ListMenu'

import Members from './members/Members'

const Headline = (props) => {
    const [view, setView] = useState([])

    useEffect(() => {
        let url = process.env.REACT_APP_API_URL + '/list/get/' + props.listId
        
        axios.get(url).then(response => {
            setView(
                <div className="position-relative index-350">
                    <div className="flex-row justify-between items-start">
                        <div>
                            <ListInformations listId={props.listId} list={response.data.data} />
                            <ListMenu listId={props.listId} list={response.data.data} />
                        </div>
    
                        <Members listId={props.listId} />
                    </div>
                </div>
            )
        })
    }, [props.listId])
    
    return view
}

export default Headline