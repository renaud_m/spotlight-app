import React, { useEffect, useState } from 'react'

import EditableField from '@superGlobals/EditableField'

const ListName = (props) => {
	const [value, setValue] = useState(props.name)

	useEffect(() => {
		document.title = value
	}, [value])

	return (
		<EditableField
			tag="h1"
			value={props.name}
			dataTable="lists"
			datafield="name"
			dataId={props.listId}
			className="font-xl-700"
			bind={setValue}
			empty={false}
		/>
	)
}

export default ListName