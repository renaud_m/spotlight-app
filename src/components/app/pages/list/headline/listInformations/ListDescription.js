import React from 'react'

import EditableField from '@superGlobals/EditableField'

const ListDescription = (props) => (
	<EditableField
		tag="p"
		textfield="textarea"
		value={props.description}
		dataTable="lists"
		datafield="description"
		className="pv-s"
		dataId={props.listId}
		placeholder="Ici la description de la liste"
	/>
)

export default ListDescription