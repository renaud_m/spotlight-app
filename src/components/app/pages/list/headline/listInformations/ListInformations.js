import React from 'react'

import ListName from './ListName'
import ListGroup from './ListGroup'

const ListInformations = (props) => (
	<div className="mb-m flex-column items-start align-left">
		<ListGroup list={props.list} />
		<ListName name={props.list.name} listId={props.listId} />
	</div>
)

export default ListInformations