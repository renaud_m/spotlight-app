

import React, { useState, useEffect } from 'react'
import axios from 'axios'

const ListGroup = (props) => {
    const [name, setName] = useState('')

    useEffect(() => {
        if (props.list.length === undefined) {
            axios.get('http://localhost:3333/group/get/' + props.list.group_id).then(response => setName(response.data.data.name))
        }
    }, [props.list])

    return <span className="w-100 font-400 alpha-50 mb-m">{name}</span>
}

export default ListGroup