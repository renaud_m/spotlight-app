import React from 'react'

const LinkItem = (props) => <a href={props.value}>{props.value}</a>

export default LinkItem