import React from 'react'
import { FieldContainer } from '../styles/Styles'

import EditableField from '@superGlobals/EditableField'

const NumberItem = (props) => (
    <FieldContainer>
        <EditableField
            tag="span"
            value={props.value}
            after={props.unit && ' ' + props.unit}
            dataTable="number_values"
            datafield="value"
            dataId={props.id}
            empty={false}
        />
    </FieldContainer>
)

export default NumberItem