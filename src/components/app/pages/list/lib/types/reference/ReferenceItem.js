import React, { useEffect, useState } from 'react'
import axios from 'axios'

import styled from 'styled-components'

const Reference = styled.div`
    background-color: #e7e7e7;
    color: #000;
    transition: background-color .3s ease;
`

const ReferenceItem = (props) => {
    const [items, setItems] = useState([])
    const [content, setContent] = useState([])

    useEffect(() => {
        axios.get('http://localhost:3333/list/' + props.listId + '/reference/all/' + props.attrId + '/' + props.id).then(response => {
            if (response.data.data.attributeReference !== null) {
                let pivotAttrId = response.data.data.attributeReference.pivot_attribute_id

                axios.get('http://localhost:3333/list/line/getReferenceItems/' + pivotAttrId + '/' + props.line).then(response => {
                    for (let i = 0; i < response.data.data.length; i++) {
                        let referenceLine = response.data.data[i].reference_line
    
                        axios.get('http://localhost:3333/list/line/get/' + referenceLine + '/' + props.listId).then(response => {
                            let item = response.data.data[0]
                            setItems(items => [...items, item])
                        })
                    }
                })
            }
        })
    }, [props.attrId, props.listId, props.id, props.line])

    useEffect(() => {
        if (items.length === 0 ) {
            setContent(
                <span className="font-m-400">Aucune entrée</span>
            )
        } else {
            setContent(
                items.map(
                    (data, index) => (
                        <Reference 
                            key={index}
                            children={data.value}
                            className="pv-t ph-s rounded font-s-400"
                        />
                    )
                )
            )
        }
    }, [items, props.listId])

    return (
        <div className="flex-row justify-start mbox-rs">
            {content}
        </div>
    )
}

export default ReferenceItem