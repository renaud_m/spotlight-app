import React, { useEffect, useState } from 'react'

import TypedIcon from '@lib/utils/TypedIcon'
import TypedValue from '@lib/utils/TypedValue'

const ReferenceLine = (props) => {
    const [content, setContent] = useState([])

    useEffect(() => {
        setContent(
            props.data.map((data, index) => (
                (index !== 0)
                &&
                <div key={index}>
                    <div className="flex-row justify-start items-center mb-s">
                        <TypedIcon type={data.attribute_type} className="mr-t" />
                        <h2 className="font-s-700">{data.column_name}</h2>
                    </div>
                    <TypedValue
                        type={data.attribute_type}
                        data={data}
                        id={props.id}
                        attrId={props.attrId}
                        listId={props.listId}
                        line={data.line}
                    />
                </div>
            ))
        )
    }, [props.data, props.attrId, props.id, props.listId])

    return (
        <section className="border-g rounded-m p-m">
            <div className="mb-m w-100">
                <h1 className="font-m-700">{props.data[0].value}</h1>
            </div>
            <div className="mbox-rm flex-row justify-start items-start wrap">
                {content}
            </div>
        </section>
    )
}

export default ReferenceLine