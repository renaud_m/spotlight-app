import React from 'react'
import { FieldContainer } from '../styles/Styles'

import EditableField from '@superGlobals/EditableField'

const TextItem = (props) => (
    <FieldContainer>
        <EditableField
            tag="span"
            value={props.value}
            dataTable="text_values"
            datafield="value"
            dataId={props.id}
            empty={false}
        />
    </FieldContainer>
)
export default TextItem