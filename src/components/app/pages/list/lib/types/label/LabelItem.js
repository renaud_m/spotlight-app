import React from 'react'
import styled from 'styled-components'

import LabelMenu from '@lib/menus/labelMenu/LabelMenu'

import { labelsColorPalette } from '@lib/palettes/LabelsColorPalette'
  
const Label = styled.label(props => ({
    backgroundColor: labelsColorPalette[props.color].main,
    color: labelsColorPalette[props.color].contrast,
}))

const LabelItem = (props) => {
    return (
        <LabelMenu
            trigger={
                <Label color={props.color} className="ph-s pv-t font-s-700 rounded cursor-pointer">
                    {props.value}
                </Label>
            }
            id={props.id}
            attrId={props.attrId}
        />

    )
}

export default LabelItem