import React from 'react'
import styled from 'styled-components'

import EditableField from '@superGlobals/EditableField'

const Percentage = styled.div`
    position: relative;
	z-index: 0;
    background-color: #f2f2f2;
    font-weight: ${props => (props.width === 100) ? 700 : 400};
    color: ${props => (props.width === 100) ? `#fff` : `#000`};

	&:before {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		background-color: #41DFA4;
		height: 100%;
		width: ${props => props.width + '%'};
		z-index: -1;
		transition: width .3s ease;
        border-radius: inherit;
	}
`

const PercentageItem = (props) => (
	<EditableField
		render={<Percentage width={props.value} className="ph-s pv-t rounded w-100" />}
		value={props.value}
		after="%"
		mode="dark"
		dataTable="percentage_values"
		datafield="value"
		className="w-100"
		dataId={props.id}
	/>
)
export default PercentageItem