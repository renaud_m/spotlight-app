import React from 'react'

const EmailItem = (props) => <a href={'email:' + props.value}>{props.value}</a>

export default EmailItem