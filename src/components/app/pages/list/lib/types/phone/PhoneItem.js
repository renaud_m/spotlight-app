import React from 'react'

const PhoneItem = (props) => <a href={'tel:' + props.value}>{props.value}</a>

export default PhoneItem