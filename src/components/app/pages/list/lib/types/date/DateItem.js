import React from 'react'

const DateItem = (props) => {

    const convert = (dateString) => {
		const date = new Date(dateString)
		let m = ("0" + (date.getMonth() + 1)).slice(-2)
		let d = ("0" + date.getDate()).slice(-2)

		return [date.getFullYear(), m, d].join("/")
	}

    return (
        <span>{convert(props.value)}</span>
    )
}

export default DateItem