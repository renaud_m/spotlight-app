import React, { useContext } from 'react'

import AppContext from '@context/AppContext'

import ModalTitle from './ModalTitle'
import ModalContent from './ModalContent'

const LineView = (props) => {
    const context = useContext(AppContext)

    const handleClick = (listId, line) => {
        context.app.setModal(true,
            <ModalTitle listId={listId} line={line} />,
            <ModalContent
                listId={listId}
                line={line}
            />,
            undefined,
            'h-100'
        )
    }

    return React.cloneElement(props.trigger, { onClick: () => handleClick(props.listId, props.line) })
}

export default LineView