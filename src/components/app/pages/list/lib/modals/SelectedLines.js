import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Spinner from '@superGlobals/Spinner'

import ReferenceLine from '@lib/types/reference/ReferenceLine'

import RippleSurface from '@superGlobals/RippleSurface'
import LineView from '@lib/modals/LineView'

const SelectedLines = (props) => {
    const [selectedLines, setSelectedLines] = useState([])
    const [content, setContent] = useState(<Spinner size="32" color="black" />)

    useEffect(() => {
        axios.get('http://localhost:3333/list/attribute/getReference/' +  props.attrId + '/' + props.id).then(response => {
            let pivotListId = response.data.data.attributeReference.pivot_list_id
            let selectedItems = response.data.data.selectedItems

            if (selectedItems.length === 0) {
                setContent(<span>Aucune entrée</span>)
            }

            for (let i = 0; i < selectedItems.length; i++) {
                const selectedItem = selectedItems[i]

                let referenceLine = selectedItem.reference_line

                axios.get('http://localhost:3333/list/line/get/' + referenceLine + '/' + pivotListId).then((response) => {
                    let data = response.data.data

                    setSelectedLines( selectedLines => [
                            ...selectedLines,
                            <LineView
                                key={i}
                                trigger={
                                    <RippleSurface mode="dark" className="cursor-pointer hover-bg-wd rounded-m">
                                        <ReferenceLine
                                            data={data}
                                            listId={props.listId}
                                            attrId={props.attrId}
                                            id={props.id}
                                        />
                                    </RippleSurface>
                                }
                                listId={pivotListId}
                                line={referenceLine}
                            />
                        ]
                        
                    )
                })
            }
        })
    }, [props.attrId, props.listId, props.id])

    useEffect(() => {
        if (selectedLines.length > 0) setContent(selectedLines)
    }, [selectedLines])

    return content
}

export default SelectedLines