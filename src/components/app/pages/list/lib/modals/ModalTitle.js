import React, { useEffect, useState } from 'react'
import axios from 'axios'

import { FieldContainer } from '../types/styles/Styles'

import EditableField from '@superGlobals/EditableField'

const ModalTitle = (props) => {
    const [data, setData] = useState('Chargement des données...')

    useEffect(() => {
        axios.get('http://localhost:3333/list/line/get/' + props.line + '/' + props.listId).then((response) => {
            let line = response.data.data
            setData(line[0])
        })
    }, [props.line, props.listId])

    return (
        <FieldContainer>
            <EditableField
                tag="h1"
                value={data.value}
                dataTable={data.attribute_type + '_values'}
                datafield="value"
                dataId={data.id}
                className="font-l-700"
                empty={false}
            />
        </FieldContainer>
    )
}

export default ModalTitle