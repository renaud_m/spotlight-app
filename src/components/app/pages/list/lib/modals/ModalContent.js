import React, { useEffect, useState } from 'react'
import axios from 'axios'

import TypedIcon from '@lib/utils/TypedIcon'
import TypedValue from '@lib/utils/TypedValue'
import SelectedLines from '@lib/modals/SelectedLines'

import Spinner from '@superGlobals/Spinner'
import EditableField from '@superGlobals/EditableField'

const ModalContent = (props) => {
    let [content, setContent] = useState(<Spinner size="64" color="black" />)

    useEffect(() => {
        const getValues = (lineDatas) => {
            let result = []

            for (let i = 1; i < lineDatas.length; i++) {
                let data = lineDatas[i]
    
                result.push(
                    <div key={i}>
                        <div className="flex-row justify-start items-center mb-s">
                            <TypedIcon type={data.type} className="mr-t" />
                            <EditableField
                                tag="h2"
                                value={data.column_name}
                                dataTable="attributes"
                                datafield="name"
                                dataId={data.attributeId}
                                className="font-s-700"
                                empty={false}
                            />
                        </div>
                        <div className="mbox-bs">
                            {
                                (data.type !== 'reference')
                                ?
                                <TypedValue
                                    type={data.type}
                                    data={data}
                                    id={data.id}
                                    attrId={data.attributeId}
                                />
                                :
                                <SelectedLines
                                    listId={props.listId}
                                    attrId={data.attributeId}
                                    id={data.id}
                                />
                            }
                        </div>
                    </div>
                )
            }
    
            return result
        }

        axios.get('http://localhost:3333/list/line/get/' + props.line + '/' + props.listId).then((response) => {
            let line = response.data.data

            setContent(
                <div className="mbox-bm">
                    {getValues(line)}
                </div>
            )
        })
    }, [props.line, props.listId])

    return content
}

export default ModalContent