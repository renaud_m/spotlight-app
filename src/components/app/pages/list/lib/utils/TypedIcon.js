import React from 'react'

import {
    MdTextFields,
    MdDateRange,
    MdLabel,
    MdLocationSearching,
    MdLink,
    MdEmail,
    MdPhone
} from 'react-icons/md'

import { TiSortNumerically } from 'react-icons/ti'

import { FaPercentage } from 'react-icons/fa'

const TypedIcon = (props) => {
    const getIconByType = (type) => {
        let IconByType = {
            text: <MdTextFields />,
            number: <TiSortNumerically />,
            date: <MdDateRange />,
            label: <MdLabel />,
            reference: <MdLocationSearching />,
            percentage: <FaPercentage />,
            link: <MdLink />,
            email: <MdEmail />,
            phone: <MdPhone />
        }

        let icon = React.cloneElement(IconByType[type], { className: props.className })

        return icon
    }

    return getIconByType(props.type)
}

export default TypedIcon