import React from 'react'
import TypedIcon from '@lib/utils/TypedIcon'

const TypedAttribute = (props) => (
    React.cloneElement(
        <div className={'flex-row justify-start items-center' + (props.className && ' ' + props.className)}>
            <TypedIcon type={props.type} className="mr-t" />
            <props.tag className="font-s-700">{props.name}</props.tag>
        </div>,
        [props]
    )
)

export default TypedAttribute