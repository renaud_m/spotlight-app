import React from 'react'

import TextItem from '@lib/types/text/TextItem'
import NumberItem from '@lib/types/number/NumberItem'
import DateItem from '@lib/types/date/DateItem'
import LabelItem from '@lib/types/label/LabelItem'
import ReferenceItem from '@lib/types/reference/ReferenceItem'
import PercentageItem from '@lib/types/percentage/PercentageItem'
import LinkItem from '@lib/types/link/LinkItem'
import EmailItem from '@lib/types/email/EmailItem'
import PhoneItem from '@lib/types/phone/PhoneItem'

const TypedValue = (props) => {
    const getComponentByType = (type, data) => {
        let componentByType = {
            text: {
                component: (
                    <TextItem
                        value={data.value}
                        color={data.color}
                        important={data.important}
                        id={data.id}
                    />
                )
            },
            number: {
                component: (
                    <NumberItem
                        value={data.value}
                        color={data.color}
                        unit={data.unit}
                        id={data.id}
                    />
                )
            },
            date: {
                component: (
                    <DateItem
                        value={data.value}
                        id={data.id}
                    />
                )
            },
            label: {
                component: (
                    <LabelItem
                        value={data.value}
                        color={data.color}
                        id={data.id}
                        attrId={props.attrId}
                    />
                ) 
            },
            reference: {
                component: (
                    <ReferenceItem
                        value={data.value}
                        listId={props.listId}
                        attrId={props.attrId}
                        id={props.id}
                        line={props.line}
                    />
                )
            },
            email: {
                component: (
                    <EmailItem
                        value={data.value}
                        id={data.id}
                    />
                ) 
            },
            phone: {
                component: (
                    <PhoneItem
                        value={data.value}
                        id={data.id}
                    />
                ) 
            },
            link: {
                component: (
                    <LinkItem
                        value={data.value}
                        id={data.id}
                    />
                )
            },
            percentage: {
                component: (
                    <PercentageItem
                        value={data.value}
                        id={data.id}
                    />
                )
            }
        }
    
        return React.cloneElement(componentByType[type].component, [props])
    }

    return getComponentByType(props.type, props.data)
}

export default TypedValue