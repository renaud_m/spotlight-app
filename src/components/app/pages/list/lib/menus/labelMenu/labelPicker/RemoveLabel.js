import React, { useContext } from 'react'
import axios from 'axios'
import { MdDelete } from "react-icons/md"

import RippleSurface from '@superGlobals/RippleSurface'

import AppContext from '@context/AppContext'

import { getContrastColorFromPalette } from '@lib/palettes/LabelsColorPalette'

const RemoveLabel = (props) => {
    const context = useContext(AppContext)

    const mode = (getContrastColorFromPalette(props.color) === '#fff') ? 'light' : 'dark'

    const handleClick = () => {
        axios.delete('http://localhost:3333/label/delete/' + props.id)
             .then((response) => context.app.insertApiResponse(response.data.query_name))
    }

    return (
        <RippleSurface
            mode={mode}
            className={'md-circle-' + mode}
            onClick={handleClick}
            children={
                <MdDelete
                    color={getContrastColorFromPalette(props.color)}
                />
            }
        />
    )
}


export default RemoveLabel