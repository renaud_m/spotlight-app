import React, { useState } from 'react'
import axios from 'axios'

import TextField from '@superGlobals/TextField'
import Button from '@superGlobals/Button'

import LabelColors from './LabelColors'

const AddForm = (props) => {

    const [value, setValue] = useState()
    const [selectedColor, setSelectedColor] = useState()

    const AddItem = () => {
        if (selectedColor !== undefined && value !== undefined) {
            axios.post(
                'http://localhost:3333/label/add',
                {attribute_id: props.attrId, color: selectedColor, value: value}
            ).then((response) => props.context.app.insertApiResponse(response.data.query_name))

            props.menuHistory.goBack()
        }
    }

    return (
        <>
            <TextField
                label="Nom de l'étiquette"
                className="mb-m"
                onChange={event => setValue(event.target.value)}
            />

            <LabelColors
                value={value}
                selectedColor={selectedColor}
                setSelectedColor={setSelectedColor}
            />

            <Button
                importance="primary"
                text="Ajouter l'étiquette"
                className="w-100"
                onClick={AddItem}
            />
        </>
    )
}


export default AddForm