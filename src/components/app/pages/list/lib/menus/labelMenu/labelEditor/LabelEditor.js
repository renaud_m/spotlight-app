import React, { useContext } from 'react'

import AppContext from '@context/AppContext'

import AddForm from './AddForm'
import EditForm from './EditForm'

const LabelEditor = (props) => {
    const context = useContext(AppContext)

    return (props.mode === 'add') ?
        <AddForm
            context={context}
            menuHistory={props.menuHistory}
            attrId={props.attrId}
        />
        :
        <EditForm
            context={context}
            menuHistory={props.menuHistory}
            defaultValue={props.defaultValue}
            defaultColor={props.defaultColor}
            targetId={props.targetId}
            id={props.id}
            attrId={props.attrId}
        />
}


export default LabelEditor