import React from 'react'

import LabelPicker from './labelPicker/LabelPicker'

import MenuDialog from '@superGlobals/MenuDialog'

const LabelMenu = (props) => (
    <MenuDialog
        title="Étiquettes"
        content={<LabelPicker id={props.id} attrId={props.attrId} />}
        trigger={props.trigger}
        className="h-100"
        contentClassName="p-m w-100"
        headline
    />
)

export default LabelMenu