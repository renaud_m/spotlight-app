import React, { useState, useEffect } from 'react'
import styled from 'styled-components'

import { MdCheck } from "react-icons/md"

import { labelsColorPalette } from '@lib/palettes/LabelsColorPalette'

const Label = styled.div`
    position: relative;
    background-color: ${props => labelsColorPalette[props.color].main};
    color: ${props => labelsColorPalette[props.color].contrast};
    height: 30px;
    width: 100%;
    line-height: 30px;
    font-weight: 700;
    transition: transform .3s ease;

    &:hover { transform: translateY(-5px) }
`

const LabelEditor = (props) => {
    const [labels, setLabels] = useState()

    useEffect(() => {
        const getLabels = () => {
            let result = []

            for (let color in labelsColorPalette) {
                result.push(
                    <div key={color} className="col-12 m:col-6" onClick={() => props.setSelectedColor(color)}>
                        <Label color={color} className="rounded cursor-pointer">
                            {props.value}
                            {
                                (props.selectedColor === color || props.defaultColor === color) &&
                                <MdCheck className="position-absolute-vr mh-s" />
                            }
                        </Label>
                    </div>
                )
            }

            setLabels(
                <div className="mb-m">
                    <div className="grid-s">
                        {result}
                    </div>
                </div>
            )
        }
        
        getLabels()
    }, [props.value, props.selectedColor, props])

    return (
        <>
            {labels}
        </>
    )
}


export default LabelEditor