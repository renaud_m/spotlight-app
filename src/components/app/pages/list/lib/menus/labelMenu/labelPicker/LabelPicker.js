import React, { useEffect, useState, useContext } from 'react'
import axios from 'axios'
import styled from 'styled-components'

import AppContext from '@context/AppContext'

import { labelsColorPalette } from '@lib/palettes/LabelsColorPalette'
import Button from '@superGlobals/Button'

import LabelEditor from '../labelEditor/LabelEditor'

import RemoveLabel from './RemoveLabel'
import EditLabel from './EditLabel'

const Options = styled.div`
    opacity: 0;
    transform: translate(100%, -50%);
    transition: all .3s ease;
`

const Container = styled.div`
    overflow: hidden;

    &:hover ${Options} {
        opacity: 1;
        transform: translate(0%, -50%);
    }
`

const Label = styled.div`
    background-color: ${props => labelsColorPalette[props.color].main};
    color: ${props => labelsColorPalette[props.color].contrast};
    font-weight: 700;
`

const LabelPicker = (props) => {
    const [labels, setLabels] = useState()
    const context = useContext(AppContext)

    useEffect(() => {
        const getLabels = () => {
            axios.get('http://localhost:3333/label/getAll/' + props.attrId).then(response => {
                (response.data.data.length > 0) ?
                setLabels(
                    response.data.data.map((data, index) => (
                        <div key={index} className="col-12 m:col-6" onClick={() => handleClick(data.color, data.value)}>
                            <Container className="flex-row justify-center position-relative">
                                <Label
                                    color={data.color}
                                    className="rounded p-s w-100 cursor-pointer"
                                    children={data.value}
                                />

                                <Options className="position-absolute-vr flex-row justify-start items-center mr-s">
                                    <EditLabel
                                        targetId={props.id}
                                        id={data.id}
                                        menuHistory={props.menuHistory}
                                        color={data.color}
                                        value={data.value}
                                    />
                                    <RemoveLabel
                                        id={data.id}
                                        color={data.color}
                                    />
                                </Options>
                            </Container>
                        </div>
                    ))
                ) :
                setLabels(<span className="font-m-400">Aucune étiquette ajouté</span>)
            })
        }

        const handleClick = (color, value) => {
            axios.put('http://localhost:3333/label/edit/' + props.id, { color: color, value: value }
            ).then((response) => context.app.insertApiResponse(response.data.query_name))
        }
        
        getLabels()
    }, [props.id, props.attrId, context.app, props.menuHistory])

    return (
        <>
            <div className="mb-m">
                <div className="grid-s">
                    {labels}
                </div>
            </div>

            <Button
                importance="primary"
                text="Ajouter une étiquette"
                className="w-100"
                onClick={
                    () => props.menuHistory.push(<LabelEditor mode="add" attrId={props.attrId} />)
                }
            />
        </>
    )
}


export default LabelPicker