import React, { useState } from 'react'
import axios from 'axios'

import TextField from '@superGlobals/TextField'
import Button from '@superGlobals/Button'

import LabelColors from './LabelColors'

const EditForm = (props) => {

    const [value, setValue] = useState(props.defaultValue)
    const [selectedColor, setSelectedColor] = useState(props.defaultColor)

    const EditItem = () => {
        const data = {
            color: selectedColor,
            value: value,
            targetId: props.targetId           
        }

        if (selectedColor !== undefined && value !== undefined) {
            axios.put('http://localhost:3333/label/edit/default/' + props.id + '/' + props.defaultColor + '/' + props.defaultValue, data)
                .then((response) => props.context.app.insertApiResponse(response.data.query_name))

            props.menuHistory.goBack()
        }
    }

    return (
        <>
            <TextField
                label="Nom de l'étiquette"
                className="mb-m"
                onChange={event => setValue(event.target.value)}
                value={value}
            />

            <LabelColors
                value={value}
                selectedColor={selectedColor}
                setSelectedColor={setSelectedColor}
            />

            <Button
                importance="primary"
                text="Modifier l'étiquette"
                className="w-100"
                onClick={EditItem}
            />
        </>
    )
}


export default EditForm