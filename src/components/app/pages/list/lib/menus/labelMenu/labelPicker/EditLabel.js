import React from 'react'
import { MdEdit } from "react-icons/md"

import RippleSurface from '@superGlobals/RippleSurface'

import LabelEditor from '../labelEditor/LabelEditor'

import { getContrastColorFromPalette } from '@lib/palettes/LabelsColorPalette'

const EditLabel = (props) => {
    const mode = (getContrastColorFromPalette(props.color) === '#fff') ? 'light' : 'dark'

    return (
        <RippleSurface
            mode={mode}
            className={'md-circle-' + mode}
            onClick={() => props.menuHistory.push(<LabelEditor mode="edit" targetId={props.targetId} id={props.id} defaultColor={props.color} defaultValue={props.value} />)}
            children={
                <MdEdit
                    color={getContrastColorFromPalette(props.color)}
                />
            }
        />
    )
}


export default EditLabel