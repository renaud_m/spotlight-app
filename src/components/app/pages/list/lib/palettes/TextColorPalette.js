export const colorPalette = {
    black: { id: 1, main: '#000', contrast: '#fff' },
    green: { id: 2, main: '#41DFA4', contrast: '#fff' },
    orange: { id: 3, main: '#ff8100', contrast: '#fff' },
    red: { id: 4, main: '#df4141', contrast: '#fff' },
    fushia: { id: 5, main: '#df41d4', contrast: '#fff' },
    blue: { id: 6, main: '#3434fd', contrast: '#fff' },
    grey: { id: 7, main: '#8c8c8c', contrast: '#fff' }
}

export const getCustomColorFromProps = (color) => {
    return colorPalette[color].main
}