export const labelsColorPalette = {
    blue: { main: '#41bddf', contrast: '#fff' },
    lightBlue: { main: '#41dfcf', contrast: '#fff' },
    green: { main: '#41DFA4', contrast: '#fff' },
    mustard: { main: '#c7df41', contrast: '#fff' },
    yellow: { main: '#dfd741', contrast: '#fff' },
    orange: { main: '#df9841', contrast: '#fff' },
    red: { main: '#df5341', contrast: '#fff' },
    grey: { main: '#efefef', contrast: '#000' }
}

export const getCustomColorFromProps = (color) => {
    return labelsColorPalette[color].main
}

export const getContrastColorFromPalette = (color) => {
    return labelsColorPalette[color].contrast
}