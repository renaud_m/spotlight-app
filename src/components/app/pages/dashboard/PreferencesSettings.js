import React from 'react'

const PreferencesSettings = (props) => {
	return (
        <div className="col-6 bg-white shadow-t rounded p-m col-auto">
            <div className="border-b mb-m pb-m">
                <span className="font-l-700">Préférences</span>
            </div>

            <div className="flex mbox-rm">
                <div>
                    <div>Prénom</div>
                    <span className="font-m-700">Renaud</span>
                </div>

                <div>
                    <div>Nom</div>
                    <span className="font-m-700">Monell</span>
                </div>
                <div>
                    <div>Email</div>
                    <span className="font-m-700">renaud.monell@gmail.com</span>
                </div>
            </div>
        </div>
	)
}


export default PreferencesSettings