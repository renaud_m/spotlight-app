import React from 'react'

import GeneralSettings from './GeneralSettings'

const Dashboard = () => {
	return (
		<div className="ph-m h-100">
		<h1 className="font-xl-700 mb-l">Mon compte</h1>
			
			<div className="grid-m h-100">
                <GeneralSettings />
			</div>
		</div>
	)
}


export default Dashboard