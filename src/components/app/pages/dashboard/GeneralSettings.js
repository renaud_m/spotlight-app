import React, { useState, useEffect } from 'react'
import axios from 'axios'

const GeneralSettings = (props) => {

    const [data, setData] = useState([])

	useEffect(() => {
		const token = localStorage.getItem('spotlight-token')
		const headers = { headers: { Authorization: `Bearer ${token}` } }

        let url = process.env.REACT_APP_API_URL + '/user/getDatas'
		
		axios.get(url, headers).then(response => setData(response.data.data))
    }, [])
    
	return (
        <div className="col-12 bg-white shadow-t rounded p-m">
            <div>
                <div className="border-b mb-m pb-m">
                    <span className="font-l-700">Général</span>
                </div>

                <div className="flex mbox-rm">
                    <div>
                        <div>Prénom</div>
                        <span className="font-m-700">{data.firstname}</span>
                    </div>
                
                    <div>
                        <div>Nom</div>
                        <span className="font-m-700">{data.lastname}</span>
                    </div>
                    <div>
                        <div>Email</div>
                        <span className="font-m-700">{data.email}</span>
                    </div>
                 </div>
            </div>

        </div>
	)
}


export default GeneralSettings