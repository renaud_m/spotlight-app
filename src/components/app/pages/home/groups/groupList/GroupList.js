import React from 'react'

import EditableField from '@superGlobals/EditableField'
import Lists from './lists/Lists'
import GroupOptions from './groupOptions/GroupOptions'

import AddGroup from './AddGroup'

const GroupList = (props) => (
    <>
        {
            props.groups.map((group, index) => (
                <section className="col-12 l:col-6" key={index}>
                    <div className="bg-white rounded-m p-m position-relative">
                        <GroupOptions
                            index={index}
                            groupId={group.id}
                            groups={props.groups}
                            setGroups={props.setGroups}
                        />

                        <div className="mb-m">
                            <EditableField
                                tag="h1"
                                value={group.name}
                                dataTable="groups"
                                datafield="name"
                                dataId={group.id}
                                className="font-l-700"
                                empty={false}
                            />
                        </div>


                        <div>
                            <Lists groupId={group.id} />
                        </div>
                    </div>
                </section>
            ))
        }

        <AddGroup setGroups={props.setGroups} />
    </>
)


export default GroupList