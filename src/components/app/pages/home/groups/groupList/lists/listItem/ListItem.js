import React, { useState } from 'react'
import styled from 'styled-components'

import { Link } from 'react-router-dom'

import EditableField from '@superGlobals/EditableField'

import ListOptions from './ListOptions'

const OptionsContainer = styled.div`
    position: absolute;
    top: 5px;
    right: 5px;
    display: ${props => props.menuOpen ? `initial` : `none`};
`

const List = styled.div`
    position: relative;
    width: 86px;
    height: 86px;

    &:hover ${OptionsContainer} { display: initial }
`

const Name = styled.span`
    font-size: 24px
`

const ListItem = (props) => {
    const [name, setName] = useState(props.name)
    const [menuOpen, setMenuOpen] = useState(false)

    return (
        <div className="flex-column items-center">
            <List className="mb-s cursor-pointer">
                <Link
                    to={'/list/' + props.groupId +'/' + props.id}
                    className="ink-clear bg-secondary flex-row justify-center items-center w-100 h-100 hover-bg-sl rounded-m"
                    children={<Name className="color-secondary-c font-700">{name[0]}</Name>}
                />
                <OptionsContainer menuOpen={menuOpen}>
                    <ListOptions
                        listId={props.id}
                        menuOpen={menuOpen}
                        setMenuOpen={setMenuOpen}
                        lists={props.lists}
                        setLists={props.setLists}
                        index={props.index}
                    />
                </OptionsContainer>
            </List>

            <EditableField
                tag="span"
                value={props.name}
                dataTable="lists"
                datafield="name"
                dataId={props.id}
                className="font-m-400"
                bind={setName}
                empty={false}
            />
        </div>
    )
}

export default ListItem