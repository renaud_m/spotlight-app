import React from 'react'
import axios from 'axios'

import { MdDelete } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'

const DeleteList = (props) => {
    const handleCLick = () => {
        let url = process.env.REACT_APP_API_URL + '/list/delete/' + props.listId

       axios.delete(url).then((response) => {
            props.lists.splice(props.index, 1)
            props.setLists([...props.lists])
        })
    }

    return (
        <RippleSurface mode="dark" transfert>
            <div className="p-s hover-bg-wd flex-row justify-start items-center rounded" onClick={handleCLick}>
                <MdDelete className="mr-s" />
                <span className="font-m-700">Supprimer</span>
            </div>
        </RippleSurface>
    )
}

export default DeleteList