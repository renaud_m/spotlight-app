import React from 'react'

import { MdMoreVert } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'
import MenuDialog from '@superGlobals/MenuDialog'

import DeleteList from './DeleteList'

const ListItem = (props) => {
    return (
        <div onClick={() => props.setMenuOpen(props.menuOpen ? false : true)}>
            <MenuDialog
                trigger={
                    <RippleSurface
                        mode="light" 
                        className="md-circle-light color-secondary-c"
                        children={<MdMoreVert />}
                    />
                }
                content={
                    <DeleteList
                        listId={props.listId}
                        setLists={props.setLists}
                        lists={props.lists}
                        index={props.index}
                    />
                }
            />
        </div> 
    )
}

export default ListItem