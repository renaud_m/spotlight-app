import React, { useState, useEffect } from 'react'
import axios from 'axios'

import ListItem from './listItem/ListItem'
import AddList from './AddList'

const Lists = (props) => {
	const [lists, setLists] = useState([])

	useEffect(() => {
		const token = localStorage.getItem('spotlight-token')
		const headers = { headers: { Authorization: `Bearer ${token}` } }

		let url = process.env.REACT_APP_API_URL + '/list/all/' + props.groupId

		axios.get(url, headers).then(response => setLists(response.data.data))
	}, [props.groupId])

	return (
		<div className="mbox-rm flex-row">
			{
				lists.map((list, index) => (
					<ListItem
						key={list.id}
						index={index}
						name={list.name}
						id={list.id}
						groupId={props.groupId}
						lists={lists}
						setLists={setLists}
					/>
				))
			}
			<AddList setLists={setLists} groupId={props.groupId} />
		</div>
	)
}


export default Lists