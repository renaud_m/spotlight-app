import React from 'react'
import axios from 'axios'

import styled from 'styled-components'

import { MdAdd } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'

const AddContainer = styled.div`
    width: 86px;
    height: 86px;
    background-color: #eee;

    svg { font-size: 24px }
`

const AddList = (props) => {
	const handleClick = () => {
        const token = localStorage.getItem('spotlight-token')
		const headers = { headers: { Authorization: `Bearer ${token}` } }

        let url = process.env.REACT_APP_API_URL + '/list/add/' + props.groupId

		axios.post(url, headers).then((response) => {
            let list = response.data.data

            props.setLists(lists => [...lists, list])
        })
	}


	return (
        <div className="flex-column items-center">
            <RippleSurface  mode="dark" transfert>
                <AddContainer
                    className="rounded-m mb-s flex-row justify-center items-center hover-bg-wd"
                    onClick={handleClick}
                    children={<MdAdd />}
                />
            </RippleSurface>

            <span>Ajouter une liste</span>
        </div>
	)
}


export default AddList