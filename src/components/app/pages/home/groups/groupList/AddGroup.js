import React from 'react'
import axios from 'axios'

import { MdAddCircle } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'


const AddGroup = (props) => {
	const handleClick = () => {
		const token = localStorage.getItem('spotlight-token')
		const data = {}
		const headers = { headers: { Authorization: `Bearer ${token}` } }

		let url = process.env.REACT_APP_API_URL + '/group/add'

		axios.post(url, data, headers).then((response) => {
			props.setGroups(groups => [...groups, response.data.data])
		})
	}


	return (
		<RippleSurface mode="dark" onClick={handleClick} className="col-12 l:col-6 bg-primary hover-bg-pd rounded-m flex-row justify-center items-center cursor-pointer">
			<div className="color-primary-c flex-row items-center p-l">
				<MdAddCircle className="mr-t" />
				<span className="font-l-700 uppercase">Ajouter un groupe</span>
			</div>
		</RippleSurface>
	)
}


export default AddGroup