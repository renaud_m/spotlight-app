import React from 'react'
import { MdMoreHoriz } from 'react-icons/md'

import MenuDialog from '@superGlobals/MenuDialog'
import RippleSurface from '@superGlobals/RippleSurface'

import DeleteGroup from './DeleteGroup'

const GroupOptions = (props) => (
    <MenuDialog
        trigger={
            <RippleSurface
                mode="dark"
                className='md-circle-dark'
                children={<MdMoreHoriz />}
            />
        }
        content={
            <DeleteGroup
                groupId={props.groupId}
                groups={props.groups}
                setGroups={props.setGroups}
                index={props.index}
            />
        }
        className="position-absolute-tr m-m"
    />
)

export default GroupOptions