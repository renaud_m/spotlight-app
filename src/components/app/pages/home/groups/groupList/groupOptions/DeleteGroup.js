import React from 'react'
import axios from 'axios'
import { MdDelete } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'


const DeleteGroup = (props) => {
    const handleClick = () => {
        props.groups.splice(props.index, 1)
        props.setGroups([...props.groups])

        axios.delete(process.env.REACT_APP_API_URL + '/group/' + props.groupId + '/delete')
    }

    return (
        <RippleSurface mode="dark" className="p-s flex-row justify-start items-center hover-bg-wd" onClick={handleClick}>
            <MdDelete />
            <span className="font-m-700 ml-s">Supprimer</span>
        </RippleSurface>
    )
}

export default DeleteGroup