import React, { useState, useEffect } from 'react'
import axios from 'axios'

import GroupList from './groupList/GroupList'

const Home = (props) => {
	const [groups, setGroups] = useState([])

	useEffect(() => {
		const token = localStorage.getItem('spotlight-token')
		const headers = { headers: { Authorization: `Bearer ${token}` } }

		let url = process.env.REACT_APP_API_URL + '/group/all'

		axios.get(url, headers).then(response => setGroups(response.data.data))
	}, [])

	return (
        <div className="w-100 h-100">
            <div className="flex-column items-start h-100 mb-m">
				<h1 className="font-xl-700 text-uppercase mb-m">Groupes</h1>
				<div className="grid-m w-100">
					<GroupList setGroups={setGroups} groups={groups} />
				</div>
            </div>
        </div>
	)
}


export default Home