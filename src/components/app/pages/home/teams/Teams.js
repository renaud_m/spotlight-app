import React, { useState, useEffect } from 'react'
import axios from 'axios'

import TeamList from './teamList/TeamList'
import AddTeam from './AddTeam'

const Teams = (props) => {
	const [teams, setTeams] = useState([])

	useEffect(() => {
		const token = localStorage.getItem('spotlight-token')
		const headers = { headers: { Authorization: `Bearer ${token}` } }

        let url = process.env.REACT_APP_API_URL + '/teams/all'

		axios.get(url, headers).then(response => setTeams(response.data.data))
	}, [])

	return (
        <div className="w-100 h-100">
            <div className="flex-column-start h-100 mbox-rm mb-m">
                <h1 className="font-xl-700 text-uppercase mb-m">Mes Équipes</h1>
                <TeamList teams={teams} />
            </div>

            <div>
                <AddTeam setTeams={setTeams} />
            </div>
        </div>
	)
}


export default Teams