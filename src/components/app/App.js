import React from 'react'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Layout from './layout/Layout'
import { AppContainer, HomeContainer } from './layout/global/Styles'

import Dashboard from './pages/dashboard/Dashboard'
import List from './pages/list/List'
import Groups from './pages/home/groups/Groups'
import Teams from './pages/home/teams/Teams'

import NotFound from '../notFound/NotFound'

import Modal from '@superGlobals/Modal'

const App = (props) => {
	const generateLayout = (WrappedComponent, navBar) => {
		return (
			<>
				<div className="position-relative index-450">
					<Layout navBar={navBar} />
				</div>
				<AppContainer navBar={navBar}>
					{WrappedComponent}
				</AppContainer>
				<Modal />
			</>		
		)
	}

	const generateHomeLayout = (WrappedComponent) => {
		return (
			<>
				<div className="position-relative index-450">
					<Layout navBar={false} />
				</div>
				<HomeContainer navBar={false}>
					<div className="flex-row justify-end align-start">
						{WrappedComponent}
					</div>
				</HomeContainer>
				<Modal />
			</>		
		)
	}

	return (
		<Router>
			<Switch>
				<Route path="/dashboard">{generateLayout(<Dashboard />, false)}</Route>

				<Route exact path="/">{generateHomeLayout(<Groups />)}</Route>
				<Route path="/list/all">{generateHomeLayout(<Groups />)}</Route>

				<Route path="/teams">{generateHomeLayout(<Teams />)}</Route>

				<Route
					path="/list/:groupId/:listId"
					render={ 
						props => generateLayout(
							<List
								groupId={props.match.params.groupId}
								listId={props.match.params.listId}
							/>, true
						)
					}
				/>

				<Route component={NotFound} />
			</Switch>
		</Router>
	)
}

export default App