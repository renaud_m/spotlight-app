import React, { useState, useEffect } from 'react'

import Auth from './auth/Auth'
import App from './app/App'

import AppContext from '@context/AppContext'

const Root = (props) => {
	const [userLogged, setUserLogged] = useState(localStorage.getItem('spotlight-token') ? true : false)
	const [lastApiResponse, setLastApiResponse] = useState(false)
	const [modalData, setModalData] = useState({})

	const [content, setContent] = useState([])

	useEffect(() => {
		const toggleUserLogged = (status) => setUserLogged(status)
		const insertApiResponse = (response) => setLastApiResponse(response)

		const setModal = (state, title, component, params, className) => {
			setModalData (
				{
					isModalOpen: state,
					modalTitle: title,
					modalContent: component,
					params: params,
					className: className
				}
			)
		}

		const appData = {
			user: {
				isLogged: userLogged,
				toggleStatus: toggleUserLogged
			},
			app: {        
				lastApiResponse: lastApiResponse,
				insertApiResponse: insertApiResponse,

				modal: modalData,
				setModal: setModal,
			}
		}

		const generateProvider = (component) => (
			<AppContext.Provider value={appData}>
				{component}
			</AppContext.Provider>
		)
	
		userLogged ? setContent(generateProvider(<App />)) : setContent(generateProvider(<Auth />))
	}, [userLogged, lastApiResponse, modalData])

	return content
}

export default Root