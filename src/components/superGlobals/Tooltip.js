import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
    position: relative;
    
    ${props => {
            if (props.direction === "right") {
                return `
                    ${StyledTooltip} {
                        top: 50%;
                        right: 0;
                        transform: translate(100%, -50%);
                    }

                    &:hover ${StyledTooltip} {
                        opacity: 1;
                        transform: translate(calc(100% + 10px), -50%);
                        pointer-events: all;
                    }

                `
            }
        }
    }
`

const StyledTooltip = styled.div`
    position: absolute;
    padding: .5rem 1rem;
    opacity: 0;
    transition: all .3s ease;
    pointer-events: none;

    &:before {
        content: '';
        position: absolute;
        top: 50%;
        left: -4px;
        transform: translateY(-50%) rotate(45deg);
        background-color: inherit;
        width: 16px;
        height: 16px;
        z-index: -1;
    }
`

const Tooltip = (props) => {
    const el = React.cloneElement(
        <Container className={props.className && props.className} direction={props.direction}>
            {props.children}
            <StyledTooltip
                type={props.type}
                className="bg-secondary color-secondary-c rounded"
                children={props.title}
            />
        </Container>,
        [props]
    )

    return el
}

export default Tooltip