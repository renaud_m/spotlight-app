import React, { useState, useRef, useEffect } from 'react'
import styled from 'styled-components'
import { MdClear, MdArrowBack } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'

const MenuContent = styled.div`
    position: absolute;
    top: ${props => props.top + 'px'};
    left: ${props => props.left + 'px'};
    background-color: #fff;
    opacity: 0;
    transform: translateY(-10px) scale(.95);
    z-index: 400;
    pointer-events: none;
    ${props => props.width && `min-width: ${props.width}px;`}
    transition: all .2s ease;

    ${props => props.isOpen && `
        pointer-events: all;
        opacity: 1;
        transform: translateY(0) scale(1);`
    }
`

const MenuDialog = (props) => {
    const menu = useRef()
    const content = useRef()

    const [contentIndex, setContentIndex] = useState('300')

    const [menuState, setMenuState] = useState(false)
    
    const [history, setHistory] = useState([props.content])
    const [component, setComponent] = useState()

    const [x, setX] = useState(0)
    const [y, setY] = useState(0)

    const backHistory = () => {
        let current = history.length - 1

        history.splice(current, 1)

        setHistory([...history])
    }

    useEffect(() => {
        const pushHistory = (component) => setHistory([...history, component])
        const backHistory = () => {
            let current = history.length - 1
    
            history.splice(current, 1)
    
            setHistory([...history])
        }

        let current = history.length - 1

        setComponent(
            React.cloneElement(
                history[current],
                { 
                    menuHistory: {
                        push: pushHistory,
                        goBack: backHistory
                    } 
                })
        )
    }, [history])

    const overflowInspector = () => {
        const trigger = menu.current.children[0]

        const triggerData = trigger.getBoundingClientRect()
        const contentData = content.current.getBoundingClientRect()

        const screen = {
            top: 0,
            right: window.innerWidth,
            left: window.offsetLeft,
            bottom: window.innerHeight + window.scrollY
        }

        if (contentData.right > screen.right) {
            let val = -Math.abs(contentData.width - triggerData.width)
            setX(val)
        }

        if (contentData.bottom > screen.bottom) {
            let val = -Math.abs(contentData.height)
            setY(val)
        } else {
            let val = triggerData.height
            setY(val)
        }
    }

    const handleClick = () => {
        overflowInspector()
        setMenuState(menuState ? false : true)
    }

    useEffect(() => setContentIndex(menuState ? '310' : '300'), [menuState])

    useEffect(() => {
        overflowInspector()

        const handleClickOutside = (event) => {
            if (menu.current && !menu.current.contains(event.target)) setMenuState(false)
        }

        document.addEventListener('mousedown', handleClickOutside)
        return () => document.removeEventListener('mousedown', handleClickOutside)
    }, [props.content])

    useEffect(() => overflowInspector(), [component])

    return (
        <div ref={menu} className={(props.className ? 'position-relative ' + props.className : 'position-relative') + ' index-' + contentIndex}>

            {React.cloneElement(props.trigger, { onClick: handleClick })}

            <MenuContent width={props.width} top={y} left={x} isOpen={menuState} ref={content} className={'rounded ' + (props.contentClassName ? props.contentClassName : '')  + ' shadow-m'}>
                {
                    (props.title)
                    &&
                    <div className="flex-row justify-between items-center mb-m pb-s border-b mbox-rm">
                        {
                            (history.length > 1)
                            &&
                            <RippleSurface mode="dark" className="back-button md-circle-dark" onClick={backHistory}>
                                <MdArrowBack />
                            </RippleSurface>
                        }
                        <span className="font-m-700">{props.title}</span>
                        <RippleSurface mode="dark" className="md-circle-dark" onClick={handleClick}>
                            <MdClear />
                        </RippleSurface>
                    </div>
                }
                {component}
            </MenuContent>
        </div>
    )
}

export default MenuDialog