import React from 'react'
import styled from 'styled-components'

const Svg = styled.svg`
    width: ${props => props.size + 'px'};
    height: ${props => props.size + 'px'};
`

const Circle = styled.circle`
    stroke: ${props => props.color};
`

const Spinner = (props) => (
    <Svg className="spinner" size={props.size} viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
        <Circle
            color={props.color}
            className="path"
            fill="none"
            strokeWidth="6"
            strokeLinecap="round"
            cx="33"
            cy="33"
            r="30"
        />
    </Svg>
)

export default Spinner