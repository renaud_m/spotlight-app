import React from 'react'
import styled, { keyframes } from 'styled-components'

const lightning = keyframes`
  from { left: 100% }

  to { left: -300% }
`;

const Highlight = styled.div`
    position: absolute;
    transition: all .3s ease;
    height: 100%;
    width: 80px;
    position: absolute;
    top: 0;
    left: 0;
    animation: none;
    background-image: linear-gradient(-90deg, transparent, rgba(255, 255, 255, .4), transparent);
    animation: ${lightning} 3s ease infinite;
` 

const Texture = styled(
    ({ component, ...props }) => React.cloneElement(component, props)
)`
    position: relative;
    overflow: hidden;
    background-color: #e2e2e2;
    ${props => props.height && `height: ${props.height}px`}
`

const LoadingTexture = (props) => (
    <Texture
        height={props.height}
        className={props.className && props.className}
        
        component={
            <props.tag
                children={<Highlight />}
            />
        }
    />
)

export default LoadingTexture