import React, { useRef, useState, useEffect } from 'react'
import styled from 'styled-components'

const Input = styled(
    ({ component, ...props }) => React.cloneElement(component, props)
)`
    ${props =>
        props.status && props.status === 'error' && `border-color: red !important`
    }
`

const Label = styled.label`
    ${props =>
        props.status && props.status === 'error' && `color: red !important`
    }
`

const ErrorMessage = styled.p`
    height: 0;
    opacity: 0;
    transition: all .3s ease;

    ${props =>
        props.status && props.status === 'error' && `
            height: 20px;
            opacity: 1;
            color: red;
        `
    }
`

const TextField = (props) => {
    const [formInputClassName, setformInputClassName] = useState('form-input')
    const input = useRef()

    const dataFilter = {
        type: (props.type) ? (props.type) : 'text',
        tagName: (props.tagName) ? props.tagName : 'input',
        className: (props.className) && props.className,
        name: (props.name) ? props.name : 'textFieldName',
        label: (props.label) ? props.label : 'TextField label here',
        value: (props.value) ? props.value : ''
    }

    useEffect(() => {
        if (input.current.value !== '') setformInputClassName('form-input is-focused')
    }, [])

    return (
        <div className={dataFilter.className}>
            <div className={formInputClassName}>
                <Label
                    htmlFor={dataFilter.name}
                    status={props.status}
                    children={dataFilter.label}
                />

                <Input
                    status={props.status}
                    className="textfield"
                
                    component={
                        <dataFilter.tagName
                            onFocus={() => setformInputClassName('form-input is-focused')}
                            onBlur={() => input.current.value === '' && setformInputClassName('form-input')}
                            name={props.name}
                            type={dataFilter.type}
                            ref={input}
                            defaultValue={dataFilter.value}
                            onChange={props.onChange}
                            onClick={props.onClick}
                            required={props.required}
                        />
                    }
                />               
            </div>
            <ErrorMessage status={props.status} children={props.errorMessage} className="mt-t" />
        </div>
    )
}

export default TextField