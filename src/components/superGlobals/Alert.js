import React, { useContext, useRef } from 'react'
import styled from 'styled-components'
import { MdWarning, MdError, MdInfo, MdClear, MdCheck } from 'react-icons/md'

import AppContext from '@context/AppContext'
import RippleSurface from '@superGlobals/RippleSurface'

const StyledAlert = styled.div`
	position: fixed;
	bottom: 40px;
	left: 40px;
	transform: translateX(calc(-100% - 40px));
	transition: transform .3s ease;
	z-index: 350;

	${props => props.isOpen && `
		transform: translateX(0);
	`}
`

const Alert = (props) => {
	let context = useContext(AppContext)
	const alert = useRef()

	const getIconByType = (type) => {
		if (type === 'warning') return <MdWarning />
		if (type === 'success') return <MdCheck />
		if (type === 'error') return <MdError />
		if (type === 'info') return <MdInfo />
	}

	return (
		<StyledAlert
			className={'p-s flex-row-start-center alert-' + context.app.alertType}
			ref={alert}
			isOpen={context.app.isAlertActive}
			children={
				<>
					<div className="flex-row-center mr-s">
						<i className="flex mr-s">{getIconByType(context.app.alertType)}</i>
						<p>{context.app.alertMessage}</p>
					</div>
					<RippleSurface
						mode="light"
						className="md-circle-light"
						children={<MdClear />}
						onClick={
							() => context.app.setAlert(
									false,
									context.app.alertType,
									context.app.alertMessage
								)
						}
					/>
				</>
			}
		/>
	)
}

export default Alert