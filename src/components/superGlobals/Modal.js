import React, { useContext, useEffect } from 'react'
import { MdClose } from "react-icons/md"
import styled from 'styled-components'

import AppContext from '@context/AppContext'
import RippleSurface from '@superGlobals/RippleSurface'
import Button from '@superGlobals/Button'

const StyledModal = styled.section`
    position: fixed;
    top: 50%;
    left: 50%;
    max-width: 760px;
    width: calc(100% - 40px);
    max-height: calc(100% - 80px);
    background-color: #fff;
    box-shadow: 0 5px 20px rgba(0, 0, 0, .3);
    z-index: 800;
`

const Overlay = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #000;
    opacity: .5;
    z-index: 750;
`

const ModalContainer = styled.div`
    position: relative;
    opacity: 0;
    pointer-events: none;
    z-index: 999;
    transition: opacity .3s ease;

    & > ${StyledModal} {
        transform: translate(-50%, -60%);
        transition: transform .3s ease;
    }

    ${props => (props.isOpen) && `
        opacity: 1;
        pointer-events: all;

        & > ${StyledModal} {
            transform: translate(-50%, -50%);
        }
    `}
`

const Modal = (props) => {
    const context = useContext(AppContext)
    const app = context.app
    const modal = app.modal

    useEffect(() => {
        document.addEventListener(
            'keydown',
            (event) => (event.keyCode === 27) && app.setModal(false, modal.modalTitle, modal.modalContent, modal.params, modal.className)
        )
    }, [app, modal.modalTitle, modal.modalContent, modal.params, modal.className])

    const closeModal = () => app.setModal(false, modal.modalTitle, modal.modalContent, modal.params, modal.className)

    return (
        <ModalContainer isOpen={modal.isModalOpen}>
            <StyledModal className={'flex rounded-m p-m ' + modal.className}>
                <div className="position-relative w-100 p-m overflow-y-auto" >
                    <RippleSurface
                        mode="dark"
                        className="md-circle-dark position-absolute-tr m-m cursor-pointer"
                        onClick={closeModal}
                    >
                        <MdClose />
                    </RippleSurface>
                        
                    <h1 className="font-l-700 text-uppercase mb-m">{modal.modalTitle}</h1>
                    {modal.modalContent}
                    {
                        (modal.params !== undefined && modal.params.type === 'confirm')
                        &&
                        <div className="flex-row mt-m mbox-rm">
                            <Button
                                importance="primary"
                                text="Oui"
                                onClick={modal.params.callBack}
                            />
                            <Button
                                importance="secondary"
                                text="Non"
                                onClick={closeModal}
                            />
                        </div>
                    }
                </div>
            </StyledModal>
            <Overlay onClick={closeModal}/>
        </ModalContainer>
    )
}

export default Modal