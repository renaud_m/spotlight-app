import React, { useRef, useEffect, useState, useContext } from 'react'
import styled from 'styled-components'
import axios from 'axios'

import AppContext from '@context/AppContext'

const Value = styled(
    ({ component, ...props }) => React.cloneElement(component, props)
)`
    ${props => props.after &&`
        &:after {
            content: ' ${props.after}'
        }
    `}
`

const EditableField = (props) => {
    const input = useRef()
    const context = useContext(AppContext)

    const [focus, setFocus] = useState(false)
    const [inputValue, setInputValue] = useState(props.value)

    const defaultValue = props.value

    const dataFilter = {
        className: (props.className !== undefined) ? props.className : '',
        mode: (props.mode !== undefined) ? props.mode : 'dark',
        tag: (props.tag) ? props.tag : 'div',
        textfield: (props.textfield) ? props.textfield : 'input',
        type: (props.type) ? props.type : 'text'
    }

    const className = {
        focused: `editable-field is-active ${dataFilter.mode} ${dataFilter.className}`,
        noFocused: `editable-field ${dataFilter.mode} ${dataFilter.className}`,
    }

    const query = (data) => {
        if (inputValue === '' && props.empty !== undefined && !props.empty) {
            setInputValue(defaultValue)
            setFocus(false)
        } else {
            let url = process.env.REACT_APP_API_URL + '/list/value/edit/' + props.dataTable + '/' + props.datafield + '/' + props.dataId
            
            axios.put(url, { value: data } ).then((response) => {
                context.app.insertApiResponse(response.data.query_name)
                if (props.bind) props.bind(data)
            })

            setFocus(false)
        }
    }

    const handleKeyPress = (event) => {
        if (event.key === 'Enter' || event.which === 13) query(inputValue)
    }

    useEffect(() => {
        if (focus) {
            input.current.select()
            input.current.focus()
        }
    }, [focus])

    useEffect(() => {
        setInputValue(props.value)
    }, [props.value])

    const el = (props.render !== undefined) ?
        React.cloneElement(
            props.render,
            [props],
            (props.placeholder && inputValue === null) ? 
            <Value after={props.after} component={<dataFilter.tag className="alpha-50" children={props.placeholder} />} />
            : 
            <Value after={props.after} component={<dataFilter.tag  children={inputValue} />} />
        ) :
        <div className={'editable-field__value '}>
            {
                (props.placeholder && inputValue === null) ?
                <Value after={props.after} component={<dataFilter.tag className="alpha-50" children={props.placeholder} />} />
                :
                <Value after={props.after} component={<dataFilter.tag children={inputValue} />} />
            }
        </div>
        
    return (
        <div className={(focus) ? className.focused : className.noFocused} onClick={() => setFocus(true)}>
            {el}
            <dataFilter.textfield
                type={dataFilter.type}
                name={props.name}
                ref={input}
                defaultValue={inputValue}
                onChange={event => setInputValue(event.target.value)}
                onBlur={() => query(inputValue)}
                onKeyPress={handleKeyPress}
                className="editable-field__input" />
        </div>
    )
}

export default EditableField