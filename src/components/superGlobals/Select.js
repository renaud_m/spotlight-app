import React, { useState, useEffect, useRef } from 'react'
import styled from 'styled-components'

import { MdArrowDropDown } from 'react-icons/md'

import RippleSurface from '@superGlobals/RippleSurface'

const StyledSelect = styled.div`
    min-width: 160px;
    background-color: #fff;
`

const SelectDropDown = styled.div`
    position: absolute;
    top: ${props => props.y + 'px'};
    left: ${props => props.x + 'px'};
    border-top: none;
    background-color: #fff;
    transition: all .3s ease;
    
    ${props => props.isSelectOpen ? `
            pointer-events: all;
            transform: translateY(0) scale(1);
            opacity: 1;
        ` :
        `
            pointer-events: none;
            transform: translateY(-5px) scale(.95);
            opacity: 0;
        `
    }
`

const DropDownContainer = styled.div`
    max-height: ${props => props.maxHeight + 'px'}
`

const DropDownOption = styled.div`
    ${props => props.isActive && `
        pointer-events: none;
        cursor: default;
    `}
`

const SelectArrow = styled.div`
    transition: transform .3s ease;
    transform: ${props => props.isSelectOpen ? `rotate(0deg)` : ` rotate(180deg)`}
`

const usePrevious = (value) => {
    const ref = useRef()

    useEffect(() => {
        ref.current = value;
    }, [value])

    return ref.current
}

const Select = (props) => {
    const [isSelectOpen, setIsSelectOpen] = useState(false)
    const [selection, setSelection] = useState(props.selected)

    const dropdown = useRef()

    const handleClick = (item) => {
        setSelection(item)
        props.onChange(item.value)
        setIsSelectOpen(false)
    }

    const getOptions = () => (
        props.items.map((item, index) => (
            (item.text.length !== undefined && typeof item.text !== 'string') ?
            <div key={index}>
                <div className="p-s w-100 align-left font-m-700">{item.value}</div>
                {
                    item.text.map((subItem, index) => (
                        <RippleSurface key={index} mode="dark" transfert>
                            <DropDownOption
                                className={'p-s hover-bg-wd cursor-pointer w-100 align-left' + (subItem.value === selection.value ? ' bg-secondary color-secondary-c' : '')}
                                onClick={() => handleClick(subItem)}
                                children={subItem.text}
                                isActive={subItem.value === selection.value ? true : false}
                            />
                        </RippleSurface>
                    ))
                }
            </div>
            :
            <RippleSurface key={index} mode="dark" transfert>
                <DropDownOption
                    className={'p-s hover-bg-wd cursor-pointer w-100 align-left' + (item.value === selection.value ? ' bg-secondary color-secondary-c' : '')}
                    onClick={() => handleClick(item)}
                    children={item.text}
                    isActive={item.value === selection.value ? true : false}
                />
            </RippleSurface>
        ))
    )

    const prevSelected = usePrevious(props.selected)

    useEffect(() => {
        if (prevSelected !== props.selected) {

        }
        
        
        setSelection(selection)
        //setSelection(props.selected)
    }, [selection, props.selected])

    useEffect(() => {
        if (prevSelected !== undefined) {
            if (prevSelected.value !== props.selected.value) {
                console.log(prevSelected)
                console.log(props.selected)
            }
        }
        
    }, [props.selected])

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (dropdown.current && !dropdown.current.contains(event.target)) setIsSelectOpen(false)
        }

        document.addEventListener('mousedown', handleClickOutside)
        return () => document.removeEventListener('mousedown', handleClickOutside)
    }, [])

    return (
        <StyledSelect ref={dropdown} className="border-g rounded-semi position-relative">
            <RippleSurface mode="dark" transfert>
                <div className="flex-row justify-between items-center rounded-semi" onClick={() => setIsSelectOpen(isSelectOpen ? false : true)}>
                    <span className="p-s">
                        {selection.text}
                    </span>
                    <SelectArrow
                        className="flex mh-s"
                        isSelectOpen={isSelectOpen}
                        children={<MdArrowDropDown />}
                    />
                </div>
            </RippleSurface>
            
            <SelectDropDown className="border-g w-100 rounded-m overflow-hidden" isSelectOpen={isSelectOpen}>
                {
                    (props.maxHeight) ?
                    <DropDownContainer
                        maxHeight={props.maxHeight}
                        className="overflow-y-auto"
                        children={getOptions()}
                    />
                    : getOptions()
                }
            </SelectDropDown>
        </StyledSelect>
    )
}

export default Select