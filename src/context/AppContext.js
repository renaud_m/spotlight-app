import React from 'react'

const AppContext = React.createContext({
    user: {
        isLogged: false,
        toggleStatus: () => {}
    },
    app: {        
        lastApiResponse: '',
        insertApiResponse: (response) => {},

        isModalOpen: false,
        modalTitle: '',
        modalContent: null,
        modalParams: {
            type: '',
            callBack: () => {}
        },
        setModal: (state, title, component) => {},

        isAlertActive: false,
        alertType: '',
        alertMessage: '',
        setAlert: (state, type, message) => {},
    }
})

export default AppContext